-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 21 Okt 2018 pada 15.18
-- Versi server: 5.7.21
-- Versi PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sap_go`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `nim` varchar(10) NOT NULL,
  `nama_mhs` varchar(100) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama_mhs`) VALUES
('1201555555', 'Mahasiswa Test');

-- --------------------------------------------------------

--
-- Struktur dari tabel `praktikum`
--

DROP TABLE IF EXISTS `praktikum`;
CREATE TABLE IF NOT EXISTS `praktikum` (
  `id_praktikum` int(11) NOT NULL AUTO_INCREMENT,
  `pr_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id_praktikum`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `praktikum`
--

INSERT INTO `praktikum` (`id_praktikum`, `pr_name`) VALUES
(1, 'SAP 01'),
(2, 'SCM100'),
(3, 'SCM300'),
(4, 'SCM550'),
(5, 'AC10'),
(6, 'AC40'),
(7, 'HR10'),
(8, 'BC400');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_menu`
--

DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE IF NOT EXISTS `system_menu` (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `id_pm` int(11) NOT NULL,
  `menu_name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `icon` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `menu_order` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `system_menu`
--

INSERT INTO `system_menu` (`id`, `parent_id`, `id_pm`, `menu_name`, `url`, `icon`, `menu_order`) VALUES
(4, 0, 2, 'Cpanel', '', 'fa fa-bars', 4),
(6, 4, 2, 'Akun', 'cpanel/account', '', 3),
(11, 4, 2, 'Hak Akses', 'cpanel/access', '', 0),
(8, 4, 2, 'Menu', 'cpanel/menu', '', 4),
(22, 21, 1, 'Mahasiswa', 'data/student', '', 0),
(20, 0, 3, 'Registrasi', '', 'fa fa-newspaper-o', 2),
(21, 0, 1, 'Data Management', '', 'fa fa-database', 1),
(25, 20, 3, 'Manajemen Jadwal', 'registrasi/jadwal', '', 0),
(29, 0, 4, 'Dashboard', 'main/dashboard', 'fa fa-desktop', 0),
(30, 21, 1, 'SAS Data Management', 'data/sas', '', 0),
(31, 21, 1, 'Payment', 'data/payment', '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_module`
--

DROP TABLE IF EXISTS `system_module`;
CREATE TABLE IF NOT EXISTS `system_module` (
  `id_pm` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pm`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_module`
--

INSERT INTO `system_module` (`id_pm`, `module_name`) VALUES
(1, 'Data Management'),
(2, 'Cpanel'),
(3, 'Registrasi'),
(4, 'Dashboard');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user`
--

DROP TABLE IF EXISTS `system_user`;
CREATE TABLE IF NOT EXISTS `system_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `display_path` varchar(255) NOT NULL,
  `level` int(2) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_user`
--

INSERT INTO `system_user` (`id_user`, `username`, `password`, `display_name`, `display_path`, `level`) VALUES
(1, 'raysoesanto', '938b4263f09b8b1dae8f027d06681ec9', 'Ray Soesanto', '/assets/images/ryp.png', 1),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '', 2),
(3, 'test', 'e10adc3949ba59abbe56e057f20f883e', 'Test Mahasiswa', '', 3),
(5, '1201555555', '789d8b9ac05ca7869f0b4ac6aa7febd9', 'Mahasiswa Test', '', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user_level`
--

DROP TABLE IF EXISTS `system_user_level`;
CREATE TABLE IF NOT EXISTS `system_user_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_user_level`
--

INSERT INTO `system_user_level` (`id_level`, `level_name`) VALUES
(1, 'Super Admin'),
(2, 'Admin '),
(3, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user_privileges`
--

DROP TABLE IF EXISTS `system_user_privileges`;
CREATE TABLE IF NOT EXISTS `system_user_privileges` (
  `id_up` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(11) NOT NULL,
  `id_pm` int(11) NOT NULL,
  PRIMARY KEY (`id_up`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_user_privileges`
--

INSERT INTO `system_user_privileges` (`id_up`, `id_level`, `id_pm`) VALUES
(1, 1, 1),
(2, 1, 2),
(11, 1, 3),
(12, 1, 4),
(13, 1, 5),
(15, 3, 4),
(16, 2, 1),
(17, 2, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user_role`
--

DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE IF NOT EXISTS `system_user_role` (
  `id_ur` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_ur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `upload_payment`
--

DROP TABLE IF EXISTS `upload_payment`;
CREATE TABLE IF NOT EXISTS `upload_payment` (
  `id_up` int(11) NOT NULL AUTO_INCREMENT,
  `id_pr` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tgl_ujian` timestamp NOT NULL,
  `nominal` int(100) NOT NULL,
  `akun` varchar(100) NOT NULL,
  `nama_file` varchar(100) NOT NULL,
  `upload_date` timestamp NOT NULL,
  `validation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_payment` varchar(100) NOT NULL,
  PRIMARY KEY (`id_up`),
  KEY `id_pr` (`id_pr`,`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `upload_payment`
--

INSERT INTO `upload_payment` (`id_up`, `id_pr`, `id_user`, `tgl_ujian`, `nominal`, `akun`, `nama_file`, `upload_date`, `validation_date`, `status_payment`) VALUES
(1, 3, 3, '0000-00-00 00:00:00', 150000, 'bank mandiri a.n Hamba Allah 135002563', 'SCM300test.jpg', '2018-08-26 09:02:24', '2018-08-26 09:18:15', 'Pembayaran Diterima'),
(3, 1, 3, '0000-00-00 00:00:00', 150000, 'bank mandiri a.n Hamba Allah 135002563', 'SAP_01test1.jpg', '2018-08-26 04:07:20', '2018-08-26 09:18:10', 'Pembayaran Diterima');

-- --------------------------------------------------------

--
-- Struktur dari tabel `upload_sas`
--

DROP TABLE IF EXISTS `upload_sas`;
CREATE TABLE IF NOT EXISTS `upload_sas` (
  `id_us` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_pr` int(11) NOT NULL,
  `tgl_ujian` timestamp NOT NULL,
  `nilai_ujian` int(3) NOT NULL,
  `nama_file` varchar(100) NOT NULL,
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipe_file` int(11) NOT NULL,
  PRIMARY KEY (`id_us`),
  KEY `id_user` (`id_user`,`id_pr`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `upload_sas`
--

INSERT INTO `upload_sas` (`id_us`, `id_user`, `id_pr`, `tgl_ujian`, `nilai_ujian`, `nama_file`, `upload_date`, `tipe_file`) VALUES
(5, 3, 3, '2018-09-01 11:51:06', 0, 'SCM300_sas_test.SAS', '2018-08-31 12:58:02', 1),
(6, 3, 3, '2018-09-01 11:51:06', 78, 'SCM300_scr_test.jpeg', '2018-09-05 04:34:19', 2),
(11, 3, 4, '2018-08-31 17:00:00', 0, 'SCM550_SAS_test.SAS', '2018-09-01 11:55:42', 1),
(12, 3, 4, '2018-08-31 17:00:00', 0, 'SCM550_SCR_test.jpeg', '2018-09-01 11:55:42', 2);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_payment`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `view_payment`;
CREATE TABLE IF NOT EXISTS `view_payment` (
`id_up` int(11)
,`id_user` int(11)
,`id_pr` int(11)
,`nama_file` varchar(100)
,`upload_date` timestamp
,`nominal` int(100)
,`akun` varchar(100)
,`status_payment` varchar(100)
,`username` varchar(255)
,`display_name` varchar(255)
,`pr_name` varchar(100)
,`tgl_ujian` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_sas`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `view_sas`;
CREATE TABLE IF NOT EXISTS `view_sas` (
`id_us` int(11)
,`id_user` int(11)
,`id_pr` int(11)
,`id_tipe_file` int(11)
,`nilai_ujian` int(3)
,`nama_file` varchar(100)
,`tgl_ujian` timestamp
,`upload_date` timestamp
,`tipe_file` varchar(3)
,`username` varchar(255)
,`display_name` varchar(255)
,`pr_name` varchar(100)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `view_payment`
--
DROP TABLE IF EXISTS `view_payment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_payment`  AS  select `a`.`id_up` AS `id_up`,`a`.`id_user` AS `id_user`,`a`.`id_pr` AS `id_pr`,`a`.`nama_file` AS `nama_file`,`a`.`upload_date` AS `upload_date`,`a`.`nominal` AS `nominal`,`a`.`akun` AS `akun`,`a`.`status_payment` AS `status_payment`,`b`.`username` AS `username`,`b`.`display_name` AS `display_name`,`c`.`pr_name` AS `pr_name`,`d`.`tgl_ujian` AS `tgl_ujian` from (((`upload_payment` `a` join `system_user` `b` on((`a`.`id_user` = `b`.`id_user`))) join `praktikum` `c` on((`a`.`id_pr` = `c`.`id_praktikum`))) join `upload_sas` `d` on((`a`.`id_pr` = `d`.`id_pr`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `view_sas`
--
DROP TABLE IF EXISTS `view_sas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_sas`  AS  select `a`.`id_us` AS `id_us`,`a`.`id_user` AS `id_user`,`a`.`id_pr` AS `id_pr`,`a`.`tipe_file` AS `id_tipe_file`,`a`.`nilai_ujian` AS `nilai_ujian`,`a`.`nama_file` AS `nama_file`,`a`.`tgl_ujian` AS `tgl_ujian`,`a`.`upload_date` AS `upload_date`,if((`a`.`tipe_file` = 1),'SAS','SCR') AS `tipe_file`,`b`.`username` AS `username`,`b`.`display_name` AS `display_name`,`c`.`pr_name` AS `pr_name` from ((`upload_sas` `a` join `system_user` `b` on((`a`.`id_user` = `b`.`id_user`))) join `praktikum` `c` on((`a`.`id_pr` = `c`.`id_praktikum`))) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
