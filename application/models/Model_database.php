<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_database extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function keprof()
	{
		$this->db
			->from('keprof');
		$query = $this->db->get();
		return $query;
	}
	function inputKep($data)
	{
		$this->db->insert('keprof', $data);
	}
	function inputNilai($data)
	{
		$this->db->insert('nilai_mk', $data);
	}
	function updateKep($data, $id) //update assisten
	{
		$this->db->update('keprof', $data, array('id_keprof' => $id));
	}

	function matkul()
	{
		$this->db
			->from('mata_kuliah');
		$query = $this->db->get();
		return $query;
	}
	function inputMk($data)
	{
		$this->db->insert('mata_kuliah', $data);
	}
	function updateMk($data, $id) //update assisten
	{
		$this->db->update('mata_kuliah', $data, array('id_mk' => $id));
	}







	//untuk dashboard//
	function ads()
	{
		$this->db
			->from('view_ads');
		$query = $this->db->get();
		return $query;
	}
	function countLate()
	{
		$this->db
			->from('view_ads')
			->where('tap_status', 'LATE');
		$query = $this->db->get();
		return $query;
	}
	function maxDPO()
	{
		$this->db
			->from('view_max_dpo');
		$query = $this->db->get();
		return $query;
	}
	function maxLate()
	{
		$this->db
			->from('view_max_terlambat');
		$query = $this->db->get();
		return $query;
	}
	function allByNIM($n)
	{
		$this->db
			->from('view_ads')
			->where('nim', $n);
		$query = $this->db->get();
		return $query;
	}


	////////////////////

	function requestAP($data)
	{
		$this->db->insert('absensi_praktikan', $data);
	}
	function requestAK($data)
	{
		$this->db->insert('absensi_kelas', $data);
	}
	function requestADetail($data)
	{
		$this->db->insert('absensi_detail', $data);
	}
	function requestAPDetail($data)
	{
		$this->db->insert('absensi_detail_praktikan', $data);
	}
	function requestAKDetail($data)
	{
		$this->db->insert('absensi_detail_kelas', $data);
	}

	function allAbsensiP()
	{
		$this->db
			->from('absensi_praktikan');
		$query = $this->db->get();
		return $query;
	}
	function allAbsensiK()
	{
		$this->db
			->from('absensi_kelas');
		$query = $this->db->get();
		return $query;
	}
	function getRFID($id)
	{
		$this->db
			->from('asisten')
			->where('rfid', $id);
		$query = $this->db->get();
		return $query;
	}
	function getRFIDP($id)
	{
		$this->db
			->from('praktikan')
			->where('rfid', $id);
		$query = $this->db->get();
		return $query;
	}

	function updatePraktikan($data, $id)
	{
		$this->db->update('praktikan', $data, array('nim' => $id));
	}
	function reqAsisten($data)
	{
		$this->db->insert('asisten', $data);
	}
	function reqPraktikan($data)
	{
		$this->db->insert('praktikan', $data);
	}
	function checkDetail($nim, $id)
	{
		$this->db
			->from('absensi_detail')
			->where('id_absensi', $id)
			->where('nim', $nim);
		$query = $this->db->get();
		return $query;
	}
	function checkDetailP($nim, $id)
	{
		$this->db
			->from('absensi_detail_praktikan')
			->where('id_absensi', $id)
			->where('nim', $nim);
		$query = $this->db->get();
		return $query;
	}
	function checkDetailK($nim, $id)
	{
		$this->db
			->from('absensi_detail_kelas')
			->where('id_absensi', $id)
			->where('nim', $nim);
		$query = $this->db->get();
		return $query;
	}
	function ReqUpdateAbsen($data, $id, $nim)
	{
		$this->db->update('absensi_detail', $data, array('id_absensi' => $id, 'nim' => $nim));
	}
	function ReqUpdateAbsenP($data, $id, $nim)
	{
		$this->db->update('absensi_detail_praktikan', $data, array('id_absensi' => $id, 'nim' => $nim));
	}
	function ReqUpdateAbsenK($data, $id, $nim)
	{
		$this->db->update('absensi_detail_kelas', $data, array('id_absensi' => $id, 'nim' => $nim));
	}
	function allTap($idabsensi)
	{
		$this->db
			->from('view_ads')
			->where('id_absensi', $idabsensi);
		$query = $this->db->get();
		return $query;
	}
	function allTapp($idabsensi)
	{
		$this->db
			->from('view_adp')
			->where('id_absensi', $idabsensi);
		$query = $this->db->get();
		return $query;
	}
	function allTapk($idabsensi)
	{
		$this->db
			->from('view_adk')
			->where('id_absensi', $idabsensi);
		$query = $this->db->get();
		return $query;
	}
	function getAbsensi($id)
	{
		$this->db
			->from('absensi')
			->where('id_absensi', $id);
		$query = $this->db->get();
		return $query;
	}
	function getAbsensiP($id)
	{
		$this->db
			->from('absensi_praktikan')
			->where('id_absensi', $id);
		$query = $this->db->get();
		return $query;
	}
	function getAbsensiK($id)
	{
		$this->db
			->from('absensi_kelas')
			->where('id_ak', $id);
		$query = $this->db->get();
		return $query;
	}
	function allAsisten()
	{
		$this->db
			->from('asisten');
		$query = $this->db->get();
		return $query;
	}
	function allPraktikan()
	{
		$this->db
			->from('praktikan');
		$query = $this->db->get();
		return $query;
	}
	function allKelas()
	{
		$this->db
			->from('kelas');
		$query = $this->db->get();
		return $query;
	}
	function getPraktikan($id)
	{
		$this->db
			->from('praktikan')
			->where('nim', $id);
		$query = $this->db->get();
		return $query;
	}
	function getKP($id)
	{
		$this->db
			->from('view_kelas_praktikan')
			->where('id_kelas', $id);
		$query = $this->db->get();
		return $query;
	}
	function getAsisten($id)
	{
		$this->db
			->from('asisten')
			->where('nim', $id);
		$query = $this->db->get();
		return $query;
	}
	function allAsistenBlank()
	{
		$this->db
			->from('asisten')
			->where('rfid', '');
		$query = $this->db->get();
		return $query;
	}
	function allPraktikanBlank()
	{
		$this->db
			->from('praktikan')
			->where('rfid', '');
		$query = $this->db->get();
		return $query;
	}
	function deleteRecordK($id)
	{
		$this->db->delete('absensi_kelas', array('id_ak' => $id));
		$this->db->delete('absensi_detail_kelas', array('id_absensi' => $id));
	}
	function deleteRecordP($id)
	{
		$this->db->delete('absensi_praktikan', array('id_absensi' => $id));
		$this->db->delete('absensi_detail_praktikan', array('id_absensi' => $id));
	}
	function deleteRecord($id)
	{
		$this->db->delete('absensi', array('id_absensi' => $id));
		$this->db->delete('absensi_detail', array('id_absensi' => $id));
	}
}
