<div class="row">
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Link RFID
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<p class="help-block">
									<b>
									Perhatian! tombol ini digunakan untuk link praktikan dengan RFID
									</b>
								</p>
								<!-- Nav tabs -->
								<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-link"></i> Link RFID</button>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Tambah Praktikan
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<p class="help-block">
									<b>
									Perhatian! tombol ini digunakan untuk menambahkan praktikan baru dalam sistem
									</b>
								</p>
								<!-- Nav tabs -->
								<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal2"><i class="fa fa-plus-circle"></i> Tambah Praktikan</button>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
</div>
<div class="row">
	<div class="col-lg-12">	
		<div class="panel panel-default">
				<div class="panel-heading">
					Data Praktikan
			</div>
			<div class="panel-body">
				<table id="example" class="display table table-hover">
					<thead>
						<tr>
							<th>NIM</th>
							<th>Nama</th>
							<th>RFID</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($a as $l){ 
						?>
						<tr>
							<td><?=$l->nim?></td>
							<td><?=$l->nama?></td>
							<td><?=$l->rfid?></td>
							<td>
							<a href="#" class="edit"  data-id="<?=$l->nim?>" data-toggle="modal" data-target="#tp"> <i class="fa fa-pencil-square-o"></i> Edit</a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>	
	</div>	
</div>	
	<style>
		.chosen-container { width: 100% !important; }
	</style>
	<div class="modal fade" id="tp" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
				<div id="data-content"></div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function(){
        $('.edit').click(function(){
            var id = $(this).attr("data-id");
            var url = '<?php echo site_url("datamgt/editPraktikan/"); ?>'+id;
            $.ajax({
                url: url,	
                type: 'get',		
                success:function(data){		
                    $('#data-content').html(data);
                }
            });
        });
    });
    </script>
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
		
			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Link RFID</h4>
			</div>
			<div class="modal-body">
					<div id="stream_list">
						<?= form_open('datamgt/reqRFIDP/');?>
							<div class="form-group">
								<p class="help-block">
								<b>
								Masukan data dengan benar.
								</b>
								</p>
							</div>
							<div class="form-group">
								<label>Praktikan</label>
								<br>
								<select data-placeholder="Pilih praktikan" name="as" class="form-control chosen-select" tabindex="2" required>
									<option value=""></option>
									<?php
									foreach($as as $a){
									?>	
									<option value="<?=$a->nim?>"><?=$a->nim?> | <?=$a->nama?></option>
									<?php } ?>
								</select>
								<p class="help-block">Pilih Asisten</p>								
							</div>	
							<div class="form-group">
								<label>RFID</label>
								<input class="form-control"  type="text" name="rfid"  required>
								<p class="help-block">Scan Kartu RFID</p>								
							</div>	
																	
					</div>
			</div>
			<div class="modal-footer">
			<button type="submit" class="btn btn-primary btn-primary  login-button">Submit</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?= form_close(); ?>
			</div>
			</div>
			
		</div>
	</div>
	<div class="modal fade" id="myModal2" role="dialog">
		<div class="modal-dialog">
		
			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Tambah Praktikan</h4>
			</div>
			<div class="modal-body">
					<div id="stream_list">
						<?= form_open('datamgt/reqPraktikan/');?>
							<div class="form-group">
								<p class="help-block">
								<b>
								Masukan data dengan benar.
								</b>
								</p>
							</div>
							<div class="form-group">
								<label>NIM</label>
								<input class="form-control"  type="text" name="nim"  required>
								<p class="help-block">Masukan NIM (10 Digit)</p>								
							</div>	
							<div class="form-group">
								<label>Nama</label>
								<input class="form-control"  type="text" name="nama"  required>
								<p class="help-block">Masukan nama praktikan</p>								
							</div>							
					</div>
			</div>
			<div class="modal-footer">
			<button type="submit" class="btn btn-primary btn-primary  login-button">Submit</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?= form_close(); ?>
			</div>
			</div>
			
		</div>
	</div>