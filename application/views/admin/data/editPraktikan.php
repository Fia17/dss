<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Edit Data Praktikan</h4>
</div>
<?= form_open('datamgt/reqEditP/');?>
<div class="modal-body">
    <div id="stream_list">
        <div class="form-group">
            <p class="help-block">
            <b>
            Masukan data dengan benar.
            </b>
            </p>
        </div>	
        <div class="form-group">
            <label>Praktikan</label>
            <input class="form-control"  type="text"  value="<?=$m->nim?> | <?=$m->nama?>" readonly>
            <input class="form-control"  type="hidden" name="nim"  value="<?=$m->nim?>" >
            <p class="help-block">Nama praktikan</p>								
        </div>                                      
        <div class="form-group">
            <label>RFID</label>
            <input class="form-control"  type="text" name="rfid"  value="<?=$m->rfid?>">
            <p class="help-block">Scan Kartu RFID</p>								
        </div>                                      
    </div>
</div>
<div class="modal-footer">
<button type="submit" class="btn btn-primary btn-primary  login-button">Submit</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <?= form_close(); ?>
</div>