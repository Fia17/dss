<script type="text/javascript">
	$('#timepicker1').timepicker();
</script>
<div class="row">
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				Input Jumlah Mata kuliah
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<p class="help-block">
					<b>
						Perhatian! tombol ini digunakan untuk memasukkan jumlah mata kuliah
					</b>
				</p>
				<!-- Nav tabs -->
				<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square-o"></i> Input Mata kuliah Available</button>
			</div>
			<!-- /.panel-body -->
		</div>
	</div>

</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Data Keprof
			</div>
			<div class="panel-body">
				<table id="example" class="display table table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Mata Kuliah</th>
							<th>Nama Mata Kuliah</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						<?php
						$no = 1;
						foreach ($mk as $k) {
						?>
							<tr>
								<td><?= $no++ ?></td>
								<td><?= $k->id_mk ?></td>
								<td><?= $k->nama_mk ?></td>
								<td>
									<a href="#" class="edit" data-id="<?= $k->id_keprof ?>" data-toggle="modal" data-target="#myModal2"> <i class="fa fa-pencil-square-o"></i> Edit</a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<style>
	.chosen-container {
		width: 100% !important;
	}
</style>
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Input Data</h4>
			</div>
			<div class="modal-body">
				<div id="stream_list">
					<?= form_open('data/inputMk/'); ?>
					<div class="form-group">
						<p class="help-block">
							<b>
								Masukan data dengan benar.
							</b>
						</p>
					</div>
					<?php
					foreach ($kep as $k) {
					?>
						<div class="form-group">
							<input type="dropdown" name="kep" value="<?= $k->id_keprof ?>">
							<label for="vehicle1"> <?= $k->nama_keprof ?></label><br>
						</div>

					<?php } ?>
					<div class="form-group">
						<label>Kode Mata Kuliah</label>
						<input class="form-control" type="text" name="kode" id="idus" required>
						<p class="help-block">Masukan kode matkul,contoh: IEH1E3</p>
					</div>
					<div class="form-group">
						<label>Nama Mata kuliah</label>
						<input class="form-control" type="text" name="name" id="idus" required>
						<p class="help-block">Masukan nama mata kuliah</p>
					</div>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-primary  login-button">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<?= form_close(); ?>
				</div>
			</div>

		</div>
	</div>
</div>