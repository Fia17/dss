<script type="text/javascript">
    $('#timepicker1').timepicker();
</script>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Pilih Mata Kuliah
            </div>
            <div class="panel-body">
                <?= form_open('data/..'); ?>
                <div class="form-group">
                 <?php
					foreach ($mk as $k) {
					?>    
                <div class="checkbox">
                       <label><input type="checkbox" value=""><?= $k->nama_mk ?></label>
                </div>
                <?php } ?>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>