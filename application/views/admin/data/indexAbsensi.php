<script type="text/javascript">
	$('#timepicker1').timepicker();
</script>
<div class="row">
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				Input Jumlah Mata kuliah
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<p class="help-block">
					<b>
						Perhatian! tombol ini digunakan untuk memasukkan jumlah mata kuliah
					</b>
				</p>
				<!-- Nav tabs -->
				<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square-o"></i> Input Jumlah Matkul</button>
			</div>
			<!-- /.panel-body -->
		</div>
	</div>

</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Data Keprof
			</div>
			<div class="panel-body">
				<table id="example" class="display table table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Keprofesian</th>
							<th>Jumlah Mata kuliah</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						<?php
						$no = 1;
						foreach ($kep as $k) {
						?>
							<tr>
								<td><?= $no++ ?></td>
								<td><?= $k->nama_keprof ?></td>
								<td><?= $k->maksimal_mk ?></td>
								<td>
									<a href="#" class="edit" data-id="<?= $k->id_keprof ?>" data-toggle="modal" data-target="#myModal2"> <i class="fa fa-pencil-square-o"></i> Edit</a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<style>
	.chosen-container {
		width: 100% !important;
	}
</style>
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Input Data</h4>
			</div>
			<div class="modal-body">
				<div id="stream_list">
					<?= form_open('data/inputKep/'); ?>
					<div class="form-group">
						<p class="help-block">
							<b>
								Masukan data dengan benar.
							</b>
						</p>
					</div>
					<div class="form-group">
						<label>Nama Kegiatan</label>
						<input class="form-control" type="text" name="kep" id="idus" required>
						<p class="help-block">Masukan nama Keprof,contoh: SCM</p>
					</div>
					<div class="form-group">
						<label>Max Mata kuliah</label>
						<input class="form-control" type="text" name="max" id="idus" required>
						<p class="help-block">Masukan jumlah maksimal mata kuliah</p>
					</div>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-primary  login-button">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<?= form_close(); ?>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript" language="javascript">
	$(document).on('click', '.edit', function() {
		var id_keprof = $(this).attr("id");
		$.ajax({
			url: "<?php echo base_url(); ?>data/updateKep/",
			method: "POST",
			data: {
				id_keprof: id_keprof
			},
			dataType: "json",
			success: function(data) {
				$('#myModal2').modal('show');
				$('#kep').val(data.kep);
				$('#max').val(data.max);
				$('.modal-title').text("Edit User");
				$('#id').val(id_keprof);
				$('#action').val("Edit");
			}
		});
	});
</script>