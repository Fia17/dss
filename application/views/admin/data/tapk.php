<?php
$attributes = array( 'id' => 'myForm');
?>
<script type="text/javascript">
window.onload=function() {
    document.getElementById('myField').oninput=function() {
        if (this.value.length >= 10) {
            document.getElementById('myForm').submit();
        }
    };
};
</script>
<div style="margin-left:10px">
	<h3>Kegiatan: <?=$a->nama_kegiatan?></h3><br>
	<h4 style="margin-top:-25px"><?=date('d F Y',strtotime($a->tgl_kegiatan))?> (<?=date('H:i',strtotime($a->jam_mulai))?> - <?=date('H:i',strtotime($a->jam_selesai))?>)</h4><br>
    <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
    <?php } ?>
    <?php if ($this->session->flashdata('warning')) { ?>
        <div class="alert alert-warning"> <?= $this->session->flashdata('warning') ?> </div>
    <?php } ?>
    <?php if ($this->session->flashdata('danger')) { ?>
        <div class="alert alert-danger"> <?= $this->session->flashdata('danger') ?> </div>
    <?php } ?>
    <h4>Silahkan Tap Kartu Anda</h4>
</div>
<?= form_open_multipart('data/requestNowK/',$attributes);?>
<input type="text" class="form-control"  name="rfid" id="myField" autofocus style="opacity:0;filter:alpha(opacity=0);">
<input type="hidden" class="form-control"  name="id" value="<?=$id?>">
<?= form_close()?>
<div class="row">
    <div class="col-lg-12">	
        <div class="panel panel-default">
                <div class="panel-heading">
                    Data Absensi Kelas	
            </div>
            <div class="panel-body">
                <table id="example" class="display table table-hover">
                    <thead>
                        <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Timestamp</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($ab as $l){ 
                            if ($l->tap_status != NULL){	
                        ?>
                        <tr>
                            <td><?=$l->nim?></td>
                            <td><?=$l->nama?></td>
                            <td><?=$l->timestamp?></td>
                            <td><?=$l->tap_status?></td>
                            
                        </tr>
                        <?php } }?>
                    </tbody>
                </table>
            </div>
        </div>	
    </div>	
</div>	