<script type="text/javascript">
            $('#timepicker1').timepicker();
        </script>
<div class="row">
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								Buat Absensi Baru
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<p class="help-block">
									<b>
									Perhatian! tombol ini digunakan untuk membuat absensi baru
									</b>
								</p>
								<!-- Nav tabs -->
								<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square-o"></i> Buat Absen baru</button>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								RFID Log
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<p class="help-block">
									<b>
									Perhatian! tombol ini digunakan untuk melihat log absensi
									</b>
								</p>
								<!-- Nav tabs -->
								<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal2"><i class="fa fa-file-text-o"></i> Lihat Log</button>
							</div>
							<!-- /.panel-body -->
						</div>
					</div>
</div>
<div class="row">
					<div class="col-lg-12">	
						<div class="panel panel-default">
							 <div class="panel-heading">
									Data Absensi	
							</div>
							<div class="panel-body">
								<table id="example" class="display table table-hover">
									<thead>
										<tr>
											<th>Nama Kegiatan</th>
											<th>Tanggal Kegiatan</th>
											<th>Jam Mulai</th>
											<th>Jam Selesai</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($ab as $l){ 
											//$q=$this->dbs->getSASByUID($l->id_user,2)->row();
										?>
										<tr>
											<td><?=$l->nama_kegiatan?></td>
											<td><?=$l->tgl_kegiatan?></td>
											<td><?=date('H:i',strtotime($l->jam_mulai))?></td>
											<td><?=date('H:i',strtotime($l->jam_selesai))?></td>
											<?php
											$end = 	date('Y-m-d H:i:s',strtotime($l->tgl_kegiatan.' '.$l->jam_selesai));
											$now =date('Y-m-d H:i:s');
											if($now>$end){
											?>
											<td>END
											| 
												<a href="<?=base_url()?>data/deleteRecordP/<?=$l->id_absensi?>"  onClick="return confirm('Apakah anda yakin?')" title="Delete Record"><i class="fa fa-trash fa-lg"></i></a>
											</td>
											<?php 
											}else{
											?>
											<td>
												<a href="<?=base_url()?>data/tapNowP/<?=$l->id_absensi?>" target="_blank" onClick="return confirm('Apakah anda yakin?')" title="Tap Now"><i class="fa fa-wifi fa-lg"></i></a> | 
												<a href="<?=base_url()?>data/deleteRecordP/<?=$l->id_absensi?>"  onClick="return confirm('Apakah anda yakin?')" title="Delete Record"><i class="fa fa-trash fa-lg"></i></a>
												
											</td>
											<?php
											}
											?>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>	
					</div>	
</div>	
<style>
					.chosen-container { width: 100% !important; }
				</style>
				<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Buat Absensi Praktikan Baru</h4>
						</div>
						<div class="modal-body">
								<div id="stream_list">
									<?= form_open('data/reqAbsensiP/');?>
										<div class="form-group">
											<p class="help-block">
											<b>
											Masukan data dengan benar.
											</b>
											</p>
										</div>
										<div class="form-group">
											<label>Nama Kegiatan</label>
											<input class="form-control"  type="text" name="keg" id="idus" required>
											<p class="help-block">Masukan nama kegiatan,contoh: Praktikum SAP01</p>								
										</div>	
										<div class="form-group">
											<label>Tanggal Kegiatan</label><br>
											<input type="date" class="form-control"  name="tgl" id="datetimepicker2">
											<p class="help-block">Tanggal pelaksanaan kegiatan</p>
										</div>	
										<label>Jam Mulai</label><br>
										<div class="input-group clockpicker data-placement="left" data-align="top" data-autoclose="true"" >
											<input type="text" class="form-control " value="09:30" name="jmulai">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
										<p class="help-block">Jam mulai pelaksanaan kegiatan</p>
										<label>Jam Selesai</label><br>
										<div class="input-group clockpicker data-placement="left" data-align="top" data-autoclose="true"" >
											<input type="text" class="form-control " value="09:30" name="jselesai">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
										<p class="help-block">Jam selesai pelaksanaan kegiatan</p>
										<div class="form-group">
											<label>Peserta</label>
											<br>
											<select data-placeholder="Pilih Peserta" name="as[]" multiple class="form-control chosen-select" tabindex="2" required>
											<option value=""></option>
												<?php
												foreach($as as $a){
												?>	
												<option value="<?=$a->nim?>"><?=$a->nama?></option>
												<?php } ?>
											</select>
											<p class="help-block">Pilih Peserta Kegiatan</p>								
										</div>										
								</div>
						</div>
						<div class="modal-footer">
						<button type="submit" class="btn btn-primary btn-primary  login-button">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						 <?= form_close(); ?>
						</div>
					  </div>
					  
					</div>
				</div>
				<div class="modal fade" id="myModal2" role="dialog">
					<div class="modal-dialog">
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Lihat Log</h4>
						</div>
						<div class="modal-body">
								<div id="stream_list">
									<?= form_open('data/rfidLogP/');?>
										<div class="form-group">
											<p class="help-block">
											<b>
											Masukan data dengan benar.
											</b>
											</p>
										</div>
										<div class="form-group">
											<label>Nama Kegiatan</label>
											<br>
											<select data-placeholder="Kegiatan" name="keg" class="form-control chosen-select" tabindex="2" required>
												<option value=""></option>
												<?php
												foreach($ab as $a){
												?>	
												<option value="<?=$a->id_absensi?>"> <?=date('d F Y',strtotime($a->tgl_kegiatan))?> <?=$a->nama_kegiatan?></option>
												<?php } ?>
											</select>
											<p class="help-block">Pilih Log Kegiatan</p>								
										</div>								
								</div>
						</div>
						<div class="modal-footer">
						<button type="submit" class="btn btn-primary btn-primary  login-button">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						 <?= form_close(); ?>
						</div>
					  </div>
					  
					</div>
				</div>