<script type="text/javascript">
    $('#timepicker1').timepicker();
</script>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Input Nilai
            </div>
            <div class="panel-body">
                <?= form_open('data/inputNilai/'); ?>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="mk">Mata kuliah:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="email" placeholder="Enter Mata Kuliah">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd">Nilai:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="pwd" placeholder="Enter Nilai">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>