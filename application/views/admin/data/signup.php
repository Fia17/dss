<div id="stream_list">
									<?= form_open('data/requestSignup/');?>
										<div class="form-group">
											<p class="help-block">
											<b>
											Masukan data dengan benar.
											</b>
											</p>
										</div>
										<div class="form-group">
											<label>Nama</label>
											<br>
											<select data-placeholder="NIM" name="nim" class="form-control chosen-select" tabindex="2" required>
												<option value=""></option>
												<?php
												foreach($pr as $a){
												?>	
												<option value="<?=$a->nim?>"><?=$a->nim?> <?=$a->nama_mhs?></option>
												<?php } ?>
											</select>
											<p class="help-block">Pilih NIM</p>								
										</div>
										<div class="form-group">
											<label>Password</label>
											<input class="form-control"  name="pass" maxlength="6" type="password" id="minle" required>
											<p class="help-block">Masukan password, min. 6 huruf</p>
										</div>
										<div class="form-group">
											<label>Ketik Ulang Password</label>
											<input class="form-control"  name="rpass" maxlength="6"  type="password" required>
											<p class="help-block">Ketik ulang password</p>
										</div>	
										<button type="submit" class="btn btn-default">Submit</button>
										<button type="reset" class="btn btn-default">Reset</button>		
									<?= form_close(); ?>										
								</div>