			<div id="footer" align="center">
				DSS &copy <?= date('Y') ?> by<a href="https://ensyselab.bie.telkomuniversity.ac.id" target="new"> Ensyse Lab | RYP</a>
			</div>
			<script type="text/javascript">
				$('.clockpicker').clockpicker();
			</script>
			<script>
				$(document).ready(function() {
					$('#example').DataTable();
				});
			</script>
			<script type="text/javascript">
				var config = {
					'.chosen-select': {},
					'.chosen-select-deselect': {
						allow_single_deselect: true
					},
					'.chosen-select-no-single': {
						disable_search_threshold: 10
					},
					'.chosen-select-no-results': {
						no_results_text: 'Oops, nothing found!'
					},
					'.chosen-select-width': {
						width: "95%"
					}
				}
				for (var selector in config) {
					$(selector).chosen(config[selector]);
				}
			</script>