<!DOCTYPE html>
<!--
Tap2Go
Build For :  School of Industrial and System Engineering Telkom University
Programmer/Creator : Ray Soesanto | RYP
Date : Jan 2019
This Information System is build based on Xrossbone System by Ray Soesanto
 ______                    ______                                                                   
(_____ \                  / _____)                                     _                            
 _____) ) _____  _   _   ( (____    ___   _____   ___  _____  ____   _| |_   ___                    
|  __  / (____ || | | |   \____ \  / _ \ | ___ | /___)(____ ||  _ \ (_   _) / _ \                   
| |  \ \ / ___ || |_| |   _____) )| |_| || ____||___ |/ ___ || | | |  | |_ | |_| |                  
|_|   |_|\_____| \__  |  (______/  \___/ |_____)(___/ \_____||_| |_|   \__) \___/                   
                (____/                                                                              
-->
<html>
	<?php $this->load->view('xrossbone/head'); ?>
	<body>
		<div id="wrapper">
			<?php $this->load->view($content); ?>
			<?php $this->load->view('xrossbone/footer'); ?>
		</div>
	</body>
</html>