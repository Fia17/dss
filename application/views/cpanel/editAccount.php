<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Edit User Account
			</div>
			<div class="panel-body">
				<div id="stream_list">
					<?= form_open('cpanel/requestEditAccount/'); ?>
					<div class="form-group">
						<p class="help-block">
							<b>
								Masukan data terkait akun yang akan dibuat di dalam sistem.
							</b>
						</p>
					</div>
					<div class="form-group">
						<label>Username</label>
						<input class="form-control" name="uname" maxlength="50" required value="<?= $acc->username ?>">
						<input class="form-control" name="id" maxlength="50" value="<?= $acc->id_user ?>" type="hidden">
						<p class="help-block">Masukan username</p>
					</div>
					<div class="form-group">
						<label>Display Name</label>
						<input class="form-control" name="dname" maxlength="15" required value="<?= $acc->display_name ?>">
						<p class="help-block">Msukan nama yang akan tertera di display</p>
					</div>
					<div class="form-group">
						<label>Keprofesian</label>
						<input class="form-control" name="keprofesian" maxlength="50" value="<?= $acc->keprofesian ?>">
						<input class="form-control" name="id" maxlength="50" value="<?= $acc->id_user ?>" type="hidden">
					</div>
					<div class="form-group">
						<label>Password Baru</label>
						<input class="form-control" name="pass" maxlength="6" type="password" id="minle" required>
						<p class="help-block">Masukan password, min. 6 huruf</p>
					</div>
					<div class="form-group">
						<label>Ketik Ulang Password Baru</label>
						<input class="form-control" name="rpass" maxlength="6" type="password" required>
						<p class="help-block">Ketik ulang password</p>
					</div>
					<div class="form-group">
						<label>Level</label>
						<select data-placeholder="Level Akses" name="lv" class="form-control chosen-select" tabindex="2" required>
							<option value="<?= $acc->id_level ?>"><?= $acc->level_name ?></option>
							<?php
							foreach ($level as $a) {
								if ($a->id_level != $acc->id_level) {
							?>
									<option value="<?= $a->id_level ?>"><?= $a->level_name ?></option>
							<?php }
							} ?>
						</select>
						<p class="help-block">Pilih Level akses</p>
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
					<button type="reset" class="btn btn-default">Reset</button>
					<?= form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>