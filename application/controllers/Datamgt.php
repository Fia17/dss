<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datamgt extends CI_Controller {
	function __construct(){
        parent::__construct();
		check_logged_in();
		$this->load->helper(array('form', 'url'));
        $this->load->model('model_cpanel');
        $this->load->model('model_database', 'dbs', TRUE);
    }

    public function asisten(){
		$data['as']=$this->dbs->allAsistenBlank()->result();
		$data['a']=$this->dbs->allAsisten()->result();
		$data['title']='Asisten';
		$data['content']='admin/database/index';			
		$this->load->view('xrossbone/index',$data);
	}
    public function praktikan(){
		$data['as']=$this->dbs->allPraktikanBlank()->result();
		$data['a']=$this->dbs->allPraktikan()->result();
		$data['title']='Praktikan';
		$data['content']='admin/database/indexP';			
		$this->load->view('xrossbone/index',$data);
	}
    function reqAsisten(){
		$dat= array(
                'nim' =>  $this->input->post('nim'),
                'nama_asisten' =>  $this->input->post('nama'),
                'kode_asisten' =>  $this->input->post('k'),
        );
		$this->dbs->reqAsisten($dat);
		redirect('datamgt/asisten','refresh');
	}
    function reqRFID(){
		$as = $this->input->post('as');
		$rfid = $this->input->post('rfid');
		$dat= array(
                'rfid' =>  $rfid,
        );
		$this->dbs->updateAsisten($dat,$as);
		redirect('datamgt/asisten','refresh');
    }
    function reqRFIDP(){
		$as = $this->input->post('as');
		$rfid = $this->input->post('rfid');
		$dat= array(
                'rfid' =>  $rfid,
        );
		$this->dbs->updatePraktikan($dat,$as);
		redirect('datamgt/praktikan','refresh');
    }
    function reqUpdateAsisten(){
        $nim=  $this->input->post('nim');
		$dat= array(
                'nama_asisten' =>  $this->input->post('nama'),
                'kode_asisten' =>  $this->input->post('k'),
        );
		$this->dbs->updateAsisten($dat,$nim);
		redirect('datamgt/asisten','refresh');
	}
	function editPraktikan($id){
		$data['m']=$this->dbs->getPraktikan($id)->row();
		$this->load->view('admin/data/editPraktikan',$data);

	}
	function reqEditP(){
		$nim = $this->input->post('nim');
		$rfid = $this->input->post('rfid');
		$dat= array(
                'rfid' =>  $rfid,
        );
		$this->dbs->updatePraktikan($dat,$nim);
		redirect('datamgt/praktikan','refresh');
    }
	function editAsisten($id){
		$data['m']=$this->dbs->getAsisten($id)->row();
		$this->load->view('admin/data/editAsisten',$data);

	}
	function reqEditA(){
		$nim = $this->input->post('nim');
		$dat= array(
				'nama_asisten' =>  $this->input->post('nama'),
				'kode_asisten' =>  $this->input->post('k'),
				'rfid' =>  $this->input->post('rfid'),
		);
		$this->dbs->updateAsisten($dat,$nim);
		redirect('datamgt/asisten','refresh');
    }
}