<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('model_cpanel');
		$this->load->model('model_database', 'dbs', TRUE);
	}
	public function absensi()
	{
		$data['kep'] = $this->dbs->keprof()->result();
		$data['title'] = 'List Keprofesian';
		$data['content'] = 'admin/data/indexAbsensi';
		$this->load->view('xrossbone/index', $data);
	}
	public function input_nilai()
	{
		//$data['nilai'] = $this->dbs->inputNilai()->result();
		$data['title'] = 'Input Nilai';
		$data['content'] = 'admin/data/inputNilai';
		$this->load->view('xrossbone/index', $data);
	}
	public function pilihMk()
	{
		$data['mk'] = $this->dbs->matkul()->result();
		$data['title'] = 'Select Mata Kuliah';
		$data['content'] = 'admin/data/vpilihMk';
		$this->load->view('xrossbone/index', $data);
	}
	public function peminatan()
	{
		//$data['nilai'] = $this->dbs->inputNilai()->result();
		$data['title'] = 'Peminatan';
		$data['content'] = 'admin/data/vpeminatan';
		$this->load->view('xrossbone/index', $data);
	}
	public function setBobot()
	{
		//$data['nilai'] = $this->dbs->inputNilai()->result();
		$data['title'] = 'Set Bobot';
		$data['content'] = 'admin/data/vbobot';
		$this->load->view('xrossbone/index', $data);
	}
	public function setmatkul()
	{
		$data['mk'] = $this->dbs->matkul()->result();
		$data['title'] = 'Set Mata Kuliah';
		$data['content'] = 'admin/data/indexSetMk';
		$this->load->view('xrossbone/index', $data);
	}
	function inputMk()
	{

		$dat = array(
			'id_mk' =>  $this->input->post('kode'),
			'id_keprof' =>  $this->input->post('kep'),
			'nama_mk' =>   $this->input->post('name'),
		);
		$this->dbs->inputMk($dat);
		// $id = $this->db->insert_id();
		// foreach ($a as $s) {
		// 	$dat2 = array(
		// 		'id_absensi' =>  $id,
		// 	);
		// 	$this->dbs->requestAdetail($dat2);
		// }
		redirect('data/absensi', 'refresh');
	}

	function inputKep()
	{

		$dat = array(
			'nama_keprof' =>  $this->input->post('kep'),
			'maksimal_mk' =>   $this->input->post('max'),
		);
		$this->dbs->inputKep($dat);
		// $id = $this->db->insert_id();
		// foreach ($a as $s) {
		// 	$dat2 = array(
		// 		'id_absensi' =>  $id,
		// 	);
		// 	$this->dbs->requestAdetail($dat2);
		// }
		redirect('data/absensi', 'refresh');
	}
	function updateKep()
	{
		$id = $this->input->post('id');
		$dat = array(
			'nama_keprof' =>  $this->input->post('kep'),
			'maksimal_mk' =>   $this->input->post('max'),
		);
		$this->dbs->updateKep($dat, $id);
		// $id = $this->db->insert_id();
		// foreach ($a as $s) {
		// 	$dat2 = array(
		// 		'id_absensi' =>  $id,
		// 	);
		// 	$this->dbs->requestAdetail($dat2);
		// }
		redirect('data/absensi', 'refresh');
	}












	public function absensiPraktikan()
	{
		$data['ab'] = $this->dbs->allAbsensiP()->result();
		$data['as'] = $this->dbs->allPraktikan()->result();
		$data['title'] = 'Absensi Praktikan';
		$data['content'] = 'admin/data/indexAbsensiP';
		$this->load->view('xrossbone/index', $data);
	}
	public function absensiKelas()
	{
		$data['ab'] = $this->dbs->allAbsensiK()->result();
		$data['as'] = $this->dbs->allPraktikan()->result();
		$data['k'] = $this->dbs->allKelas()->result();
		$data['title'] = 'Absensi Praktikan';
		$data['content'] = 'admin/data/indexAbsensiK';
		$this->load->view('xrossbone/index', $data);
	}

	function reqAbsensiP()
	{
		$keg = $this->input->post('keg');
		$tgl = $this->input->post('tgl');
		$jmulai = $this->input->post('jmulai');
		$jselesai = $this->input->post('jselesai');
		$a = $this->input->post('as');
		$dat = array(
			'nama_kegiatan' =>  $keg,
			'tgl_kegiatan' =>  $tgl,
			'jam_mulai' =>  $jmulai,
			'jam_selesai' =>  $jselesai,
		);
		$this->dbs->requestAP($dat);
		$id = $this->db->insert_id();
		foreach ($a as $s) {
			$dat2 = array(
				'id_absensi' =>  $id,
				'nim' =>  $s,
			);
			$this->dbs->requestAPdetail($dat2);
		}
		redirect('data/absensiPraktikan', 'refresh');
	}
	function reqAbsensiK()
	{
		$keg = $this->input->post('keg');
		$tgl = $this->input->post('tgl');
		$jmulai = $this->input->post('jmulai');
		$jselesai = $this->input->post('jselesai');
		$a = $this->input->post('as');
		$dat = array(
			'nama_kegiatan' =>  $keg,
			'tgl_kegiatan' =>  $tgl,
			'jam_mulai' =>  $jmulai,
			'jam_selesai' =>  $jselesai,
		);
		$this->dbs->requestAK($dat);
		$id = $this->db->insert_id();
		foreach ($a as $s) {
			$q = $this->dbs->getKP($s)->result();
			foreach ($q as $qq) {
				$dat2 = array(
					'id_absensi' =>  $id,
					'nim' =>  $qq->nim,
				);
				$this->dbs->requestAKdetail($dat2);
			}
		}
		redirect('data/absensiKelas', 'refresh');
	}
	public function tapNow($idabsensi)
	{
		$data['ab'] = $this->dbs->allTap($idabsensi)->result();
		$data['a'] = $this->dbs->getAbsensi($idabsensi)->row();
		$data['id'] = $idabsensi;
		$data['title'] = 'Absensi';
		$data['content'] = 'admin/data/tap';
		$this->load->view('xrossbone/indexTap', $data);
	}
	public function tapNowP($idabsensi)
	{
		$data['ab'] = $this->dbs->allTapp($idabsensi)->result();
		$data['a'] = $this->dbs->getAbsensiP($idabsensi)->row();
		$data['id'] = $idabsensi;
		$data['title'] = 'Absensi';
		$data['content'] = 'admin/data/tapp';
		$this->load->view('xrossbone/indexTap', $data);
	}
	public function tapNowK($idabsensi)
	{
		$data['ab'] = $this->dbs->allTapk($idabsensi)->result();
		$data['a'] = $this->dbs->getAbsensiK($idabsensi)->row();
		$data['id'] = $idabsensi;
		$data['title'] = 'Absensi';
		$data['content'] = 'admin/data/tapk';
		$this->load->view('xrossbone/indexTap', $data);
	}

	function requestNow()
	{
		$rfid = $this->input->post('rfid');
		$id = $this->input->post('id');
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');
		$checkTime = $this->dbs->getAbsensi($id)->row();
		$time = date('Y-m-d H:i:s', strtotime($checkTime->tgl_kegiatan . ' ' . $checkTime->jam_mulai . ' +15 minute'));
		//cek kalau si eta telat
		if ($time < $now) {
			//kalau si eta telat	
			$checkx = $this->dbs->getRFID($rfid)->row();
			//cek apa data rfid nya ada atau ngga
			if ($checkx != NULL) {
				//kalau data rfid si eta ada di db	
				$check2x = $this->dbs->checkDetail($checkx->nim, $id)->row();
				//cek apa si eta emang di set di absen atau ngga
				if ($check2x == NULL) {
					//kondisi kalau si eta ga ada di absen	
					redirect('data/tapNow/' . $id, 'refresh');
				} else {
					//kondisi kalau ternyata data si eta ada di absen detail	
					$dat2 = array(
						'tap_status' =>  "LATE",
						'timestamp' =>  date("Y-m-d H:i:s"),
					);
					$this->dbs->reqUpdateAbsen($dat2, $id, $checkx->nim);
					redirect('data/tapNow/' . $id, 'refresh');
				}
			} else {
				//kalau data si eta ga ada
				redirect('data/tapNow/' . $id, 'refresh');
			}
		} else {
			//kalau si eta ga telat	
			$check = $this->dbs->getRFID($rfid)->row();
			//cek apa rfid si eta ada atau ngga
			if ($check != NULL) {
				//kalau data si eta ada	
				$check2 = $this->dbs->checkDetail($check->nim, $id)->row();
				if ($check2 == NULL) {
					//kalau data si eta ternyata ga ada di absensi

					redirect('data/tapNow/' . $id, 'refresh');
				} else {
					//kalau data si eta ada di sistem berarti ga ngaret
					$dat = array(
						'tap_status' =>  "TAP",
						'timestamp' =>  date("Y-m-d H:i:s"),
					);
					$this->dbs->reqUpdateAbsen($dat, $id, $check->nim);
					redirect('data/tapNow/' . $id, 'refresh');
				}
			} else {
				//kalau data rfid si eta ga ada
				redirect('data/tapNow/' . $id, 'refresh');
			}
		}
	}
	function requestNowP()
	{
		$rfid = $this->input->post('rfid');
		$id = $this->input->post('id');
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');
		$checkTime = $this->dbs->getAbsensiP($id)->row();
		$time = date('Y-m-d H:i:s', strtotime($checkTime->tgl_kegiatan . ' ' . $checkTime->jam_mulai . ' +10 minute'));
		//cek kalau si eta telat
		if ($time < $now) {
			//kalau si eta telat	
			$checkx = $this->dbs->getRFIDP($rfid)->row();
			//cek apa data rfid nya ada atau ngga
			if ($checkx != NULL) {
				//kalau data rfid si eta ada di db	
				$check2x = $this->dbs->checkDetailP($checkx->nim, $id)->row();
				//cek apa si eta emang di set di absen atau ngga
				if ($check2x == NULL) {
					//kondisi kalau si eta ga ada di absen	
					$this->session->set_flashdata('warning', 'Si eta ga ada di absen :((');
					redirect('data/tapNowP/' . $id, 'refresh');
				} else {
					//kondisi kalau ternyata data si eta ada di absen detail	
					$dat2 = array(
						'tap_status' =>  "LATE",
						'timestamp' =>  date("Y-m-d H:i:s"),
					);
					$this->dbs->reqUpdateAbsenP($dat2, $id, $checkx->nim);
					$this->session->set_flashdata('danger', 'NGARET IH KAMUU >.<');
					redirect('data/tapNowP/' . $id, 'refresh');
				}
			} else {
				//kalau data si eta ga ada
				$this->session->set_flashdata('warning', 'Si eta data rfidnya gug ada :((');
				redirect('data/tapNowP/' . $id, 'refresh');
			}
		} else {
			//kalau si eta ga telat	
			$check = $this->dbs->getRFIDP($rfid)->row();
			//cek apa rfid si eta ada atau ngga
			if ($check != NULL) {
				//kalau data si eta ada	
				$check2 = $this->dbs->checkDetailP($check->nim, $id)->row();
				if ($check2 == NULL) {
					//kalau data si eta ternyata ga ada di absensi
					$this->session->set_flashdata('warning', 'Si eta ga ada di absen :((');
					redirect('data/tapNowP/' . $id, 'refresh');
				} else {
					//kalau data si eta ada di sistem berarti ga ngaret
					$dat = array(
						'tap_status' =>  "TAP",
						'timestamp' =>  date("Y-m-d H:i:s"),
					);
					$this->dbs->reqUpdateAbsenP($dat, $id, $check->nim);
					$this->session->set_flashdata('success', 'Si eta ga ngaret, baguss');
					redirect('data/tapNowP/' . $id, 'refresh');
				}
			} else {
				//kalau data rfid si eta ga ada
				$this->session->set_flashdata('warning', 'Si eta data rfidnya gug ada :((');
				redirect('data/tapNowP/' . $id, 'refresh');
			}
		}
	}
	function requestNowK()
	{
		$rfid = $this->input->post('rfid');
		$id = $this->input->post('id');
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');
		$checkTime = $this->dbs->getAbsensiK($id)->row();
		$time = date('Y-m-d H:i:s', strtotime($checkTime->tgl_kegiatan . ' ' . $checkTime->jam_mulai . ' +10 minute'));
		//cek kalau si eta telat
		if ($time < $now) {
			//kalau si eta telat	
			$checkx = $this->dbs->getRFIDP($rfid)->row();
			//cek apa data rfid nya ada atau ngga
			if ($checkx != NULL) {
				//kalau data rfid si eta ada di db	
				$check2x = $this->dbs->checkDetailK($checkx->nim, $id)->row();
				//cek apa si eta emang di set di absen atau ngga
				if ($check2x == NULL) {
					//kondisi kalau si eta ga ada di absen	
					$this->session->set_flashdata('warning', 'Si eta ga ada di absen :((');
					redirect('data/tapNowK/' . $id, 'refresh');
				} else {
					//kondisi kalau ternyata data si eta ada di absen detail	
					$dat2 = array(
						'tap_status' =>  "LATE",
						'timestamp' =>  date("Y-m-d H:i:s"),
					);
					$this->dbs->reqUpdateAbsenK($dat2, $id, $checkx->nim);
					$this->session->set_flashdata('danger', 'NGARET IH KAMUU >.<');
					redirect('data/tapNowK/' . $id, 'refresh');
				}
			} else {
				//kalau data si eta ga ada
				$this->session->set_flashdata('warning', 'Si eta data rfidnya gug ada :((');
				redirect('data/tapNowK/' . $id, 'refresh');
			}
		} else {
			//kalau si eta ga telat	
			$check = $this->dbs->getRFIDP($rfid)->row();
			//cek apa rfid si eta ada atau ngga
			if ($check != NULL) {
				//kalau data si eta ada	
				$check2 = $this->dbs->checkDetailK($check->nim, $id)->row();
				if ($check2 == NULL) {
					//kalau data si eta ternyata ga ada di absensi
					$this->session->set_flashdata('warning', 'Si eta ga ada di absen :((');
					redirect('data/tapNowK/' . $id, 'refresh');
				} else {
					//kalau data si eta ada di sistem berarti ga ngaret
					$dat = array(
						'tap_status' =>  "TAP",
						'timestamp' =>  date("Y-m-d H:i:s"),
					);
					$this->dbs->reqUpdateAbsenK($dat, $id, $check->nim);
					$this->session->set_flashdata('success', 'Si eta ga ngaret, baguss');
					redirect('data/tapNowK/' . $id, 'refresh');
				}
			} else {
				//kalau data rfid si eta ga ada
				$this->session->set_flashdata('warning', 'Si eta data rfidnya gug ada :((');
				redirect('data/tapNowK/' . $id, 'refresh');
			}
		}
	}

	/*
function requestNow(){
		$rfid = $this->input->post('rfid');
		$id=$this->input->post('id');
		date_default_timezone_set('Asia/Jakarta');
		$now =date('Y-m-d H:i:s');
		$checkTime = $this->dbs->getAbsensi($id)->row();
		$time = date('Y-m-d H:i:s',strtotime($checkTime->tgl_kegiatan.' '.$checkTime->jam_mulai .' +15 minute'));
		if($time<$now){
			$checkx = $this->dbs->getRFID($rfid)->row();
			if($checkx != NULL){
				$check2x = $this->dbs->checkDetail($checkx->nim,$id)->row();
				if($check2x==NULL){
						$dat2= array(
						'id_absensi' =>  $id,
						'nim' =>  $checkx->nim,
						'status' =>  "LATE",
						'timestamp' =>  date("Y-m-d H:i:s"),
					);
					$this->dbs->requestInputAbsen($dat2);
					redirect('data/tapNow/'.$id,'refresh');
				}else{
					redirect('data/tapNow/'.$id,'refresh');
				}
			}else{
				redirect('data/absensi','refresh');
			}
		}else{
			$check = $this->dbs->getRFID($rfid)->row();
			if($check != NULL){
				$check2 = $this->dbs->checkList($check->nim,$id)->row();
				if($check2==NULL){
						$dat= array(
						'id_absensi' =>  $id,
						'nim' =>  $check->nim,
						'status' =>  "TAP",
						'timestamp' =>  date("Y-m-d H:i:s"),
					);
					$this->dbs->requestInputAbsen($dat);
					redirect('data/tapNow/'.$id,'refresh');
				}else{
					redirect('data/tapNow/'.$id,'refresh');
				}
			}else{
				redirect('data/absensi','refresh');
			}
		}
		
	}
*/

	public function rfidLog()
	{
		$id = $this->input->post('keg');
		$data['q'] = $this->dbs->allTap($id)->result();
		$data['a'] = $this->dbs->getAbsensi($id)->row();
		$data['title'] = 'RFID Log';
		$data['content'] = 'admin/data/rfidLog';
		$this->load->view('xrossbone/index', $data);
	}
	public function rfidLogP()
	{
		$id = $this->input->post('keg');
		$data['q'] = $this->dbs->allTapp($id)->result();
		$data['a'] = $this->dbs->getAbsensiP($id)->row();
		$data['title'] = 'RFID Log';
		$data['content'] = 'admin/data/rfidLogP';
		$this->load->view('xrossbone/index', $data);
	}
	public function rfidLogK()
	{
		$id = $this->input->post('keg');
		$data['q'] = $this->dbs->allTapk($id)->result();
		$data['a'] = $this->dbs->getAbsensiK($id)->row();
		$data['title'] = 'RFID Log';
		$data['content'] = 'admin/data/rfidLogK';
		$this->load->view('xrossbone/index', $data);
	}
	public function deleteRecord($id)
	{
		$this->dbs->deleteRecord($id);
		redirect('data/absensi', 'refresh');
	}
	public function deleteRecordP($id)
	{
		$this->dbs->deleteRecordP($id);
		redirect('data/absensiPraktikan', 'refresh');
	}
	public function deleteRecordK($id)
	{
		$this->dbs->deleteRecordK($id);
		redirect('data/absensiKelas', 'refresh');
	}
}
