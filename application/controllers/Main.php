<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url', 'email'));
		$this->load->model('model_cpanel');
		$this->load->model('model_database', 'dbs', TRUE);
	}

	public function index()
	{
		if ($_POST == NULL) {
			$data['page_title'] = "Login | DSS";
			$data['desc'] = "";
			$this->load->view('system_login/login', $data);
		} else {

			$user = $this->input->post('textUsername');
			$pass = $this->input->post('textPassword');
			$pass5 = md5($pass);
			$q = $this->model_cpanel->checkUserLogin($user, $pass5)->row();
			if ($q->password == $pass5) {
				$session_data = array(
					'id_user' => $q->id_user,
					'username' => $q->username,
					'nama' => $q->display_name,
					'lv' => $q->id_level,
					'lvname' => $q->level_name,
					'display' => $q->display_path,
					'is_login' => TRUE
				);
				if ($q->id_level == 3 || $q->id_level == 1 || $q->id_level == 2) {
					$this->session->set_userdata($session_data);
					redirect('main/dashboard');
				}
			} else {
				redirect('main');
			}
		}
	}

	public function logout()
	{
		error_reporting(0);
		$data = array(
			'id_user' => 0,
			'username' => 0,
			'nama' => 0,
			'level' => 0,
			'display' => 0,
			'is_login' => FALSE
		);
		$this->session->sess_destroy();
		$this->session->unset_userdata($data);
		redirect('main');
		exit();
	}

	public function dashboard()
	{
		$lv = $this->session->userdata('lv');
		$id = $this->session->userdata('id_user');
		switch ($lv) {
			case 1: //Super Admin
				// $c= $this->dbs->countLate()->num_rows();	
				// $ctot= $this->dbs->ads()->num_rows();	
				// $aslate= $this->dbs->maxLate()->row();	
				// $c1=($c/$ctot)*100;
				// if($c1>0){
				// 	$pc1=$c1;
				// }else{
				// 	$pc1="";
				// }
				// $d= $this->dbs->maxDPO()->num_rows();
				// if($d>1){
				// 	$d1=$this->dbs->maxDPO()->result();
				// }else{
				// 	$d1=$this->dbs->maxDPO()->row();
				// }
				$data['title'] = 'Dashboard';
				//$data['late']=$c;	
				//$data['plate']=$pc1;	
				//$data['alate']=$aslate;	
				//$data['d']=$d;	
				//$data['d1']=$d1;	
				$data['content'] = 'admin/indexAdmin';
				break;
			case 2: //Admin
				// $c= $this->dbs->countLate()->num_rows();	
				// $ctot= $this->dbs->ads()->num_rows();	
				// $aslate= $this->dbs->maxLate()->row();	
				// $c1=($c/$ctot)*100;
				// if($c1>0){
				// 	$pc1=$c1;
				// }else{
				// 	$pc1="";
				// }
				// $d= $this->dbs->maxDPO()->num_rows();
				// if($d>1){
				// 	$d1=$this->dbs->maxDPO()->result();
				// }else{
				// 	$d1=$this->dbs->maxDPO()->row();
				// }
				$data['title'] = 'Dashboard';
				// $data['late']=$c;	
				// $data['plate']=$pc1;	
				// $data['alate']=$aslate;	
				// $data['d']=$d;	
				// $data['d1']=$d1;	
				$data['content'] = 'admin/indexAdmin';
				$data['content'] = 'admin/indexAdmin';
				break;
			default:
				$data['title'] = 'Dashboard';
				//$data['or']=$this->dbs->allOprec()->result();	
				$data['content'] = 'admin/index';
		}
		$this->load->view('xrossbone/index', $data);
	}



	public function error()
	{
		$this->load->view('system_login/error');
	}
	public function success()
	{
		$this->load->view('system_login/success');
	}
}
