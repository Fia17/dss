-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 07 Jan 2019 pada 18.07
-- Versi server: 5.7.21
-- Versi PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tap2go`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi`
--

DROP TABLE IF EXISTS `absensi`;
CREATE TABLE IF NOT EXISTS `absensi` (
  `id_absensi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kegiatan` varchar(100) NOT NULL,
  `tgl_kegiatan` date NOT NULL,
  `jam_mulai` time(4) NOT NULL,
  `jam_selesai` time(4) NOT NULL,
  PRIMARY KEY (`id_absensi`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `nama_kegiatan`, `tgl_kegiatan`, `jam_mulai`, `jam_selesai`) VALUES
(1, 'Test', '2019-01-07', '06:01:00.0000', '21:01:00.0000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi_detail`
--

DROP TABLE IF EXISTS `absensi_detail`;
CREATE TABLE IF NOT EXISTS `absensi_detail` (
  `id_ad` int(11) NOT NULL AUTO_INCREMENT,
  `id_absensi` int(11) NOT NULL,
  `angkatan` int(4) NOT NULL,
  PRIMARY KEY (`id_ad`),
  KEY `id_absen` (`id_absensi`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `absensi_detail`
--

INSERT INTO `absensi_detail` (`id_ad`, `id_absensi`, `angkatan`) VALUES
(1, 1, 2015),
(2, 1, 2016);

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi_list`
--

DROP TABLE IF EXISTS `absensi_list`;
CREATE TABLE IF NOT EXISTS `absensi_list` (
  `id_al` int(11) NOT NULL AUTO_INCREMENT,
  `id_absensi` int(11) NOT NULL,
  `nim` int(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL,
  PRIMARY KEY (`id_al`),
  KEY `id_absensi` (`id_absensi`),
  KEY `id_asisten` (`nim`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `absensi_list`
--

INSERT INTO `absensi_list` (`id_al`, `id_absensi`, `nim`, `status`, `timestamp`) VALUES
(1, 1, 120900083, 'TAP', '2019-01-07 17:53:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `asisten`
--

DROP TABLE IF EXISTS `asisten`;
CREATE TABLE IF NOT EXISTS `asisten` (
  `nim` int(11) NOT NULL,
  `nama_asisten` varchar(100) NOT NULL,
  `kode_asisten` varchar(3) NOT NULL,
  `angkatan` int(4) NOT NULL,
  `rfid` varchar(10) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `asisten`
--

INSERT INTO `asisten` (`nim`, `nama_asisten`, `kode_asisten`, `angkatan`, `rfid`) VALUES
(120900083, 'Royandi', 'ROY', 2015, '4147253717');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_menu`
--

DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE IF NOT EXISTS `system_menu` (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `id_pm` int(11) NOT NULL,
  `menu_name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `icon` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `menu_order` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `system_menu`
--

INSERT INTO `system_menu` (`id`, `parent_id`, `id_pm`, `menu_name`, `url`, `icon`, `menu_order`) VALUES
(4, 0, 2, 'Cpanel', '', 'fa fa-bars', 4),
(6, 4, 2, 'Akun', 'cpanel/account', '', 3),
(11, 4, 2, 'Hak Akses', 'cpanel/access', '', 0),
(8, 4, 2, 'Menu', 'cpanel/menu', '', 4),
(32, 21, 1, 'Buat Absensi', 'data/absensi', '', 0),
(21, 0, 1, 'Manajemen Absensi', '', 'fa fa-database', 1),
(25, 20, 3, 'Manajemen Jadwal', 'registrasi/jadwal', '', 0),
(29, 0, 4, 'Dashboard', 'main/dashboard', 'fa fa-desktop', 0),
(30, 21, 1, 'RFID Log', 'data/log', '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_module`
--

DROP TABLE IF EXISTS `system_module`;
CREATE TABLE IF NOT EXISTS `system_module` (
  `id_pm` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pm`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_module`
--

INSERT INTO `system_module` (`id_pm`, `module_name`) VALUES
(1, 'Data Management'),
(2, 'Cpanel'),
(3, 'Registrasi'),
(4, 'Dashboard');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user`
--

DROP TABLE IF EXISTS `system_user`;
CREATE TABLE IF NOT EXISTS `system_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `display_path` varchar(255) NOT NULL,
  `level` int(2) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_user`
--

INSERT INTO `system_user` (`id_user`, `username`, `password`, `display_name`, `display_path`, `level`) VALUES
(1, 'raysoesanto', '938b4263f09b8b1dae8f027d06681ec9', 'Ray Soesanto', '/assets/images/ryp.png', 1),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user_level`
--

DROP TABLE IF EXISTS `system_user_level`;
CREATE TABLE IF NOT EXISTS `system_user_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_user_level`
--

INSERT INTO `system_user_level` (`id_level`, `level_name`) VALUES
(1, 'Super Admin'),
(2, 'Admin '),
(3, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user_privileges`
--

DROP TABLE IF EXISTS `system_user_privileges`;
CREATE TABLE IF NOT EXISTS `system_user_privileges` (
  `id_up` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(11) NOT NULL,
  `id_pm` int(11) NOT NULL,
  PRIMARY KEY (`id_up`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_user_privileges`
--

INSERT INTO `system_user_privileges` (`id_up`, `id_level`, `id_pm`) VALUES
(1, 1, 1),
(2, 1, 2),
(11, 1, 3),
(12, 1, 4),
(13, 1, 5),
(15, 3, 4),
(16, 2, 1),
(17, 2, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user_role`
--

DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE IF NOT EXISTS `system_user_role` (
  `id_ur` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_ur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_aal`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `view_aal`;
CREATE TABLE IF NOT EXISTS `view_aal` (
`id_al` int(11)
,`id_absensi` int(11)
,`nim` int(10)
,`status` varchar(100)
,`timestamp` timestamp
,`nama_asisten` varchar(100)
,`kode_asisten` varchar(3)
,`angkatan` int(4)
,`rfid` varchar(10)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `view_aal`
--
DROP TABLE IF EXISTS `view_aal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_aal`  AS  select `a`.`id_al` AS `id_al`,`a`.`id_absensi` AS `id_absensi`,`a`.`nim` AS `nim`,`a`.`status` AS `status`,`a`.`timestamp` AS `timestamp`,`b`.`nama_asisten` AS `nama_asisten`,`b`.`kode_asisten` AS `kode_asisten`,`b`.`angkatan` AS `angkatan`,`b`.`rfid` AS `rfid` from (`absensi_list` `a` join `asisten` `b` on((`a`.`nim` = `b`.`nim`))) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
