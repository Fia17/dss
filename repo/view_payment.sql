-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 21 Okt 2018 pada 15.21
-- Versi server: 5.7.21
-- Versi PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sap_go`
--

-- --------------------------------------------------------

--
-- Struktur untuk view `view_payment`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_payment`  AS  select `a`.`id_up` AS `id_up`,`a`.`id_user` AS `id_user`,`a`.`id_pr` AS `id_pr`,`a`.`nama_file` AS `nama_file`,`a`.`upload_date` AS `upload_date`,`a`.`nominal` AS `nominal`,`a`.`akun` AS `akun`,`a`.`status_payment` AS `status_payment`,`b`.`username` AS `username`,`b`.`display_name` AS `display_name`,`c`.`pr_name` AS `pr_name`,`d`.`tgl_ujian` AS `tgl_ujian` from (((`upload_payment` `a` join `system_user` `b` on((`a`.`id_user` = `b`.`id_user`))) join `praktikum` `c` on((`a`.`id_pr` = `c`.`id_praktikum`))) join `upload_sas` `d` on((`a`.`id_pr` = `d`.`id_pr`))) ;

--
-- VIEW  `view_payment`
-- Data: Tidak ada
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
