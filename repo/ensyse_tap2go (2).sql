-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 28 Feb 2019 pada 04.26
-- Versi server: 5.7.24
-- Versi PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ensyse_tap2go`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi`
--

DROP TABLE IF EXISTS `absensi`;
CREATE TABLE IF NOT EXISTS `absensi` (
  `id_absensi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kegiatan` varchar(100) NOT NULL,
  `tgl_kegiatan` date NOT NULL,
  `jam_mulai` time(4) NOT NULL,
  `jam_selesai` time(4) NOT NULL,
  PRIMARY KEY (`id_absensi`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `nama_kegiatan`, `tgl_kegiatan`, `jam_mulai`, `jam_selesai`) VALUES
(2, 'Test Tap', '2019-02-26', '23:00:00.0810', '23:10:00.2920'),
(3, 'ttesr', '2019-02-28', '07:49:00.0000', '01:05:00.0000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi_detail`
--

DROP TABLE IF EXISTS `absensi_detail`;
CREATE TABLE IF NOT EXISTS `absensi_detail` (
  `id_ad` int(11) NOT NULL AUTO_INCREMENT,
  `id_absensi` int(11) NOT NULL,
  `nim` int(10) NOT NULL,
  `angkatan` int(4) NOT NULL,
  `tap_status` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL,
  PRIMARY KEY (`id_ad`),
  KEY `id_absen` (`id_absensi`),
  KEY `nim` (`nim`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `absensi_detail`
--

INSERT INTO `absensi_detail` (`id_ad`, `id_absensi`, `nim`, `angkatan`, `tap_status`, `timestamp`) VALUES
(3, 2, 1201900083, 0, 'LATE', '2019-02-26 16:04:59'),
(4, 2, 1201157899, 0, 'LATE', '0000-00-00 00:00:00'),
(5, 2, 1201150305, 0, '', '0000-00-00 00:00:00'),
(6, 3, 1201157899, 0, 'LATE', '2019-02-26 03:00:00'),
(7, 3, 1201900083, 0, 'TAP', '2019-02-26 16:04:59'),
(8, 4, 1201900083, 0, '', '2019-02-26 16:04:59'),
(9, 3, 120900083, 0, '', '0000-00-00 00:00:00'),
(10, 3, 1201157899, 0, '', '0000-00-00 00:00:00'),
(11, 3, 1201174020, 0, '', '0000-00-00 00:00:00'),
(12, 3, 1201154363, 0, '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi_list`
--

DROP TABLE IF EXISTS `absensi_list`;
CREATE TABLE IF NOT EXISTS `absensi_list` (
  `id_al` int(11) NOT NULL AUTO_INCREMENT,
  `id_absensi` int(11) NOT NULL,
  `nim` int(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL,
  PRIMARY KEY (`id_al`),
  KEY `id_absensi` (`id_absensi`),
  KEY `id_asisten` (`nim`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `asisten`
--

DROP TABLE IF EXISTS `asisten`;
CREATE TABLE IF NOT EXISTS `asisten` (
  `nim` int(11) NOT NULL,
  `nama_asisten` varchar(100) NOT NULL,
  `kode_asisten` varchar(3) NOT NULL,
  `angkatan` int(4) NOT NULL,
  `rfid` varchar(10) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `asisten`
--

INSERT INTO `asisten` (`nim`, `nama_asisten`, `kode_asisten`, `angkatan`, `rfid`) VALUES
(120900083, 'Royandi', 'ROY', 2015, '4147253717'),
(1201157899, 'Tondi Rastafara', 'TND', 2015, '0000000000'),
(1201174020, 'Nganu Hattaraitanakul', 'NGN', 0, ''),
(1201154363, 'Ajeng Kirana', 'AJE', 2015, '4148852741'),
(1201150305, 'Ayu Saskia Priscilla Harris', 'AYU', 2015, '0307905079'),
(1201150304, 'Desi Lestari', 'ICI', 2015, '1586869522'),
(1201154130, 'Dhialdi Wibi Raditya', 'ENY', 2015, '0353252455'),
(1201154432, 'Dwi Sela Tresnasari', 'SEL', 2015, '4148759605'),
(1201154097, 'Fahmy Habib Hasanudin', 'BIB', 2015, '4146789173'),
(1201154359, 'Fitra Akbarokah', 'APT', 2015, '4148554229'),
(1201154229, 'I Gusti Ngurah Bagus Arya Dananjaya', 'RAH', 2015, '0307032359'),
(1201150295, 'Liza Nafiah Maulidina', 'NAF', 2015, '3801778406'),
(1201154120, 'Muhammad Rakatama Farhan Habibie', 'RKT', 2015, '4148595301'),
(1201154268, 'Muhammad Zakiey Azhar', 'JEK', 2015, '4147082869'),
(1201150013, 'Murazky Hengki Riaja', 'MVP', 2015, '4146914981'),
(1201154463, 'Najah', 'BAA', 2015, '0492506883'),
(1201154433, 'Nancy Putri Ramadhani', 'CAY', 2015, '4148511845'),
(1201154484, 'Novita Permata Sari', 'NOV', 2015, '4145159029'),
(1201154538, 'Olivia Cahyaningdhia Saputri', 'LIV', 2015, '4149054533'),
(1201152534, 'Rahma Fauziyah', 'RFZ', 2015, '4148565109'),
(1201154105, 'Rayhan Muhammad', 'RYN', 2015, '4148308405'),
(1201154338, 'Renata Ganiswara', 'BII', 2015, '4144273637'),
(1201153537, 'Rizal Wardhani', 'BOS', 2015, '4148731509'),
(1201154365, 'Safinah Rizkiyani', 'SFN', 2015, '4147260021'),
(1201154325, 'Siska Febriyanti', 'CYK', 2015, '4145328965'),
(1201164292, 'Amanda Sucita Brahmana', 'AMD', 2016, '4148258997'),
(1201164326, 'Aqfie jumidhal Ardha', 'AFI', 2016, '4147144853'),
(1201164042, 'Arief Adhitya Rahman', 'PEP', 2016, '4146486325'),
(1201164125, 'Dina Anjani', 'SWR', 2016, '4146872309'),
(1201160452, 'Firdausa Ramadhanti', 'UUS', 2016, '4148496133'),
(1201164165, 'Iksan Budiman', 'LVR', 2016, '4148818229'),
(1201164294, 'Indah Rahayu Alika Putri', 'ALK', 2016, '4146537269'),
(1201164318, 'Mochamad Ilham Fanani', 'IFN', 2016, '4145248597'),
(1201164242, 'Mohammad Fuad Farisi', 'AIS', 2016, '4147213365'),
(1201162112, 'Msy Cahaya Dinda Pamungkas', 'MSY', 2016, '0308192535'),
(1201160303, 'Muhammad Yumna Majdina', 'MAJ', 2016, '3286426326'),
(1201164170, 'Naufal Rifat Muhammad Harris', 'RTH', 2016, '4146673525'),
(1201164402, 'Nur Fajar Fadhilah', 'NFF', 2016, '0307668055'),
(1201164137, 'Nur Fitrianita Syahnas', 'PIW', 2016, '4144639765'),
(1201164371, 'Ramadhani Kurniawan', 'RAM', 2016, '0307699255'),
(1201160338, 'Rully Satriawan', 'RUS', 2016, '4147273925'),
(1201164310, 'Wildan Chairul Faqqie', 'DAN', 2016, '4148955573'),
(1201174086, 'Alan Ramadhan', '', 2017, ''),
(1201174200, 'Arifianto Aji Saputro', '', 2017, ''),
(1201174181, 'Dimas Arvian Budianto', '', 2017, ''),
(1201184202, 'Faturochman Nirwana', '', 2017, ''),
(1201174317, 'Fitriya Ayuningtyas Rumanda', '', 2017, ''),
(1201170119, 'Jasmine Devina Fany Atmaja', '', 2017, ''),
(1201174379, 'Kevin Ahmad Aufarrizky', '', 2017, ''),
(1201174051, 'Komang Surya Gitawan Argya Putra', '', 2017, ''),
(1201174075, 'Made Arya Teguh Dvaipayana', '', 2017, ''),
(1201174063, 'Muhammad Arief Ardyansyah', '', 2017, ''),
(1201174084, 'Muhammad Rafif Aliyasyah', '', 2017, ''),
(1201174255, 'Sandy Argya Pradipta', '', 2017, ''),
(1201174313, 'Sholekah Dwi Saputri', '', 2017, ''),
(1201170332, 'Talitha Nursabila', '', 2017, ''),
(1201174240, 'Ulfia Rahmah', '', 2017, ''),
(1201170396, 'Wardatul Faizah', '', 2017, ''),
(1201184077, 'Wina Nisrina Nalini', '', 2018, ''),
(120900084, 'Royandi Jr', 'ROJ', 2015, '4147253717'),
(1201164337, 'Lazuardi Iman Mauludi', 'LIM', 0, '4146810773');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_menu`
--

DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE IF NOT EXISTS `system_menu` (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `id_pm` int(11) NOT NULL,
  `menu_name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `icon` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `menu_order` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `system_menu`
--

INSERT INTO `system_menu` (`id`, `parent_id`, `id_pm`, `menu_name`, `url`, `icon`, `menu_order`) VALUES
(4, 0, 2, 'Cpanel', '', 'fa fa-bars', 4),
(6, 4, 2, 'Akun', 'cpanel/account', '', 3),
(11, 4, 2, 'Hak Akses', 'cpanel/access', '', 0),
(8, 4, 2, 'Menu', 'cpanel/menu', '', 4),
(32, 21, 1, 'Buat Absensi', 'data/absensi', '', 0),
(21, 0, 1, 'Manajemen Absensi', '', 'fa fa-database', 1),
(25, 20, 3, 'Manajemen Jadwal', 'registrasi/jadwal', '', 0),
(29, 0, 4, 'Dashboard', 'main/dashboard', 'fa fa-desktop', 0),
(33, 0, 1, 'Manajemen Data', '', '', 2),
(34, 33, 1, 'Asisten Laboratorium', 'datamgt/asisten', '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_module`
--

DROP TABLE IF EXISTS `system_module`;
CREATE TABLE IF NOT EXISTS `system_module` (
  `id_pm` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pm`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_module`
--

INSERT INTO `system_module` (`id_pm`, `module_name`) VALUES
(1, 'Data Management'),
(2, 'Cpanel'),
(3, 'Registrasi'),
(4, 'Dashboard');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user`
--

DROP TABLE IF EXISTS `system_user`;
CREATE TABLE IF NOT EXISTS `system_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `display_path` varchar(255) NOT NULL,
  `level` int(2) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_user`
--

INSERT INTO `system_user` (`id_user`, `username`, `password`, `display_name`, `display_path`, `level`) VALUES
(1, 'raysoesanto', '938b4263f09b8b1dae8f027d06681ec9', 'Ray Soesanto', '/assets/images/ryp.png', 1),
(2, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'Admin', '', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user_level`
--

DROP TABLE IF EXISTS `system_user_level`;
CREATE TABLE IF NOT EXISTS `system_user_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_user_level`
--

INSERT INTO `system_user_level` (`id_level`, `level_name`) VALUES
(1, 'Super Admin'),
(2, 'Admin '),
(3, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user_privileges`
--

DROP TABLE IF EXISTS `system_user_privileges`;
CREATE TABLE IF NOT EXISTS `system_user_privileges` (
  `id_up` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(11) NOT NULL,
  `id_pm` int(11) NOT NULL,
  PRIMARY KEY (`id_up`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_user_privileges`
--

INSERT INTO `system_user_privileges` (`id_up`, `id_level`, `id_pm`) VALUES
(1, 1, 1),
(2, 1, 2),
(11, 1, 3),
(12, 1, 4),
(13, 1, 5),
(15, 3, 4),
(16, 2, 1),
(17, 2, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_user_role`
--

DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE IF NOT EXISTS `system_user_role` (
  `id_ur` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_ur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_ads`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `view_ads`;
CREATE TABLE IF NOT EXISTS `view_ads` (
`id_ad` int(11)
,`id_absensi` int(11)
,`nim` int(10)
,`angkatan` int(4)
,`nama_asisten` varchar(100)
,`kode_asisten` varchar(3)
,`rfid` varchar(10)
,`tap_status` varchar(50)
,`timestamp` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_asisten_dpo`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `view_asisten_dpo`;
CREATE TABLE IF NOT EXISTS `view_asisten_dpo` (
`nim` int(11)
,`nama_asisten` varchar(100)
,`kode_asisten` varchar(3)
,`angkatan` int(4)
,`rfid` varchar(10)
,`count_DPO` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_asisten_terlambat`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `view_asisten_terlambat`;
CREATE TABLE IF NOT EXISTS `view_asisten_terlambat` (
`nim` int(11)
,`nama_asisten` varchar(100)
,`kode_asisten` varchar(3)
,`angkatan` int(4)
,`rfid` varchar(10)
,`count_late` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_max_dpo`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `view_max_dpo`;
CREATE TABLE IF NOT EXISTS `view_max_dpo` (
`nim` int(11)
,`nama_asisten` varchar(100)
,`kode_asisten` varchar(3)
,`angkatan` int(4)
,`rfid` varchar(10)
,`dpo` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_max_terlambat`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `view_max_terlambat`;
CREATE TABLE IF NOT EXISTS `view_max_terlambat` (
`nim` int(11)
,`nama_asisten` varchar(100)
,`kode_asisten` varchar(3)
,`angkatan` int(4)
,`rfid` varchar(10)
,`count_late` bigint(21)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `view_ads`
--
DROP TABLE IF EXISTS `view_ads`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ads`  AS  select `a`.`id_ad` AS `id_ad`,`a`.`id_absensi` AS `id_absensi`,`a`.`nim` AS `nim`,`a`.`angkatan` AS `angkatan`,`b`.`nama_asisten` AS `nama_asisten`,`b`.`kode_asisten` AS `kode_asisten`,`b`.`rfid` AS `rfid`,`a`.`tap_status` AS `tap_status`,`a`.`timestamp` AS `timestamp` from (`absensi_detail` `a` left join `asisten` `b` on((`a`.`nim` = `b`.`nim`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `view_asisten_dpo`
--
DROP TABLE IF EXISTS `view_asisten_dpo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_asisten_dpo`  AS  select `a`.`nim` AS `nim`,`a`.`nama_asisten` AS `nama_asisten`,`a`.`kode_asisten` AS `kode_asisten`,`a`.`angkatan` AS `angkatan`,`a`.`rfid` AS `rfid`,(select count(0) from `absensi_detail` `b` where ((`b`.`nim` = `a`.`nim`) and ((`b`.`tap_status` = '') or (`b`.`tap_status` = NULL)))) AS `count_DPO` from `asisten` `a` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `view_asisten_terlambat`
--
DROP TABLE IF EXISTS `view_asisten_terlambat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_asisten_terlambat`  AS  select `a`.`nim` AS `nim`,`a`.`nama_asisten` AS `nama_asisten`,`a`.`kode_asisten` AS `kode_asisten`,`a`.`angkatan` AS `angkatan`,`a`.`rfid` AS `rfid`,(select count(0) from `absensi_detail` `b` where ((`b`.`nim` = `a`.`nim`) and (`b`.`tap_status` = 'LATE'))) AS `count_late` from `asisten` `a` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `view_max_dpo`
--
DROP TABLE IF EXISTS `view_max_dpo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_max_dpo`  AS  select `a`.`nim` AS `nim`,`a`.`nama_asisten` AS `nama_asisten`,`a`.`kode_asisten` AS `kode_asisten`,`a`.`angkatan` AS `angkatan`,`a`.`rfid` AS `rfid`,max(`a`.`count_DPO`) AS `dpo` from `view_asisten_dpo` `a` group by `a`.`nim` order by `a`.`count_DPO` desc limit 5 ;

-- --------------------------------------------------------

--
-- Struktur untuk view `view_max_terlambat`
--
DROP TABLE IF EXISTS `view_max_terlambat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_max_terlambat`  AS  select `a`.`nim` AS `nim`,`a`.`nama_asisten` AS `nama_asisten`,`a`.`kode_asisten` AS `kode_asisten`,`a`.`angkatan` AS `angkatan`,`a`.`rfid` AS `rfid`,`a`.`count_late` AS `count_late` from `view_asisten_terlambat` `a` where (`a`.`count_late` = (select max(`b`.`count_late`) from `view_asisten_terlambat` `b`)) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
