-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2019 at 04:05 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ensyse_tap2go`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id_absensi` int(11) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `tgl_kegiatan` date NOT NULL,
  `jam_mulai` time(4) NOT NULL,
  `jam_selesai` time(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `nama_kegiatan`, `tgl_kegiatan`, `jam_mulai`, `jam_selesai`) VALUES
(1, 'Test', '2019-01-07', '06:01:00.0000', '21:01:00.0000'),
(2, 'Test hari ini', '2019-02-22', '13:30:00.0000', '13:35:00.0000'),
(7, 'Piket Hari Senin', '2019-02-25', '16:02:00.0000', '17:02:00.0000'),
(8, 'Piket Hari Selasa', '2019-02-26', '17:00:00.0000', '18:02:00.0000'),
(9, 'Piket Hari Rabu', '2019-02-27', '17:00:00.0000', '17:02:00.0000'),
(10, 'Piket Hari Kamis', '2019-02-28', '16:02:00.0000', '17:02:00.0000'),
(11, 'Piket Hari Jumat', '2019-03-01', '16:02:00.0000', '17:02:00.0000');

-- --------------------------------------------------------

--
-- Table structure for table `absensi_detail`
--

CREATE TABLE `absensi_detail` (
  `id_ad` int(11) NOT NULL,
  `id_absensi` int(11) NOT NULL,
  `angkatan` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi_detail`
--

INSERT INTO `absensi_detail` (`id_ad`, `id_absensi`, `angkatan`) VALUES
(1, 1, 2015),
(2, 1, 2016),
(3, 2, 2015),
(4, 2, 2016),
(5, 3, 2016),
(6, 4, 2015),
(7, 4, 2016),
(8, 5, 2015),
(9, 5, 2016),
(10, 6, 2015),
(11, 6, 2016),
(12, 7, 2015),
(13, 7, 2016),
(14, 8, 2015),
(15, 8, 2016),
(16, 9, 2015),
(17, 9, 2016),
(18, 10, 2015),
(19, 10, 2016),
(20, 11, 2015),
(21, 11, 2016);

-- --------------------------------------------------------

--
-- Table structure for table `absensi_list`
--

CREATE TABLE `absensi_list` (
  `id_al` int(11) NOT NULL,
  `id_absensi` int(11) NOT NULL,
  `nim` int(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi_list`
--

INSERT INTO `absensi_list` (`id_al`, `id_absensi`, `nim`, `status`, `timestamp`) VALUES
(1, 1, 120900083, 'TAP', '2019-01-07 17:53:39'),
(2, 1, 120900083, 'LATE', '2019-01-07 17:53:39'),
(3, 1, 120900084, 'LATE', '2019-01-07 17:53:39'),
(4, 2, 1201164165, 'TAP', '2019-02-22 06:31:06'),
(5, 2, 1201164318, 'TAP', '2019-02-22 06:31:13'),
(6, 2, 1201164242, 'TAP', '2019-02-22 06:31:16'),
(7, 3, 1201164337, 'TAP', '2019-02-22 14:39:57'),
(8, 3, 1201164294, 'TAP', '2019-02-22 14:40:35'),
(9, 4, 1201164337, 'TAP', '2019-02-23 14:04:44'),
(10, 4, 1201164294, 'TAP', '2019-02-23 14:05:07'),
(11, 5, 1201154229, 'LATE', '2019-02-25 00:30:15'),
(12, 6, 1201154229, 'LATE', '2019-02-25 00:31:43'),
(21, 7, 1201154229, 'TAP', '2019-02-25 06:16:23'),
(14, 7, 1201154097, 'TAP', '2019-02-25 00:33:51'),
(15, 7, 1201174020, 'TAP', '2019-02-25 00:37:21'),
(16, 7, 1201164337, 'TAP', '2019-02-25 03:01:46'),
(17, 7, 1201154130, 'TAP', '2019-02-25 03:50:51'),
(18, 7, 1201164042, 'TAP', '2019-02-25 05:06:34'),
(19, 7, 1201162112, 'TAP', '2019-02-25 05:06:41'),
(20, 7, 1201154484, 'TAP', '2019-02-25 05:14:03'),
(22, 7, 1201154538, 'TAP', '2019-02-25 06:16:32'),
(23, 7, 1201164402, 'TAP', '2019-02-25 06:17:01'),
(24, 7, 1201150304, 'TAP', '2019-02-25 06:19:52'),
(25, 7, 1201150013, 'TAP', '2019-02-25 06:21:56'),
(26, 7, 1201154338, 'TAP', '2019-02-25 07:29:43'),
(27, 7, 1201153537, 'TAP', '2019-02-25 07:30:28'),
(28, 7, 1201154120, 'TAP', '2019-02-25 08:27:56'),
(29, 7, 1201164326, 'TAP', '2019-02-25 08:31:01'),
(31, 8, 1201150304, 'TAP', '2019-02-26 02:13:46'),
(32, 8, 1201154097, 'TAP', '2019-02-26 03:28:52'),
(33, 8, 1201164402, 'TAP', '2019-02-26 03:42:10'),
(34, 8, 1201154229, 'TAP', '2019-02-26 03:59:09'),
(35, 8, 1201164137, 'TAP', '2019-02-26 03:59:29'),
(36, 8, 1201154120, 'TAP', '2019-02-26 04:29:43'),
(37, 8, 1201154538, 'TAP', '2019-02-26 05:50:29'),
(38, 8, 1201153537, 'TAP', '2019-02-26 06:28:47'),
(39, 8, 1201164337, 'TAP', '2019-02-26 08:19:32'),
(40, 8, 1201160338, 'TAP', '2019-02-26 08:19:46'),
(41, 8, 1201164165, 'TAP', '2019-02-26 08:20:05'),
(42, 9, 1201160338, 'TAP', '2019-02-27 08:03:51'),
(43, 9, 1201154120, 'TAP', '2019-02-27 08:03:51'),
(44, 9, 1201153537, 'TAP', '2019-02-27 08:03:51'),
(45, 9, 1201164402, 'TAP', '2019-02-27 08:03:51'),
(46, 9, 1201164165, 'TAP', '2019-02-27 08:03:51'),
(47, 9, 1201164337, 'TAP', '2019-02-27 08:03:51'),
(48, 9, 1201154130, 'TAP', '2019-02-27 08:03:51'),
(49, 9, 1201164371, 'TAP', '2019-02-27 08:03:51'),
(50, 9, 1201150013, 'TAP', '2019-02-27 08:03:51'),
(51, 9, 1201150304, 'TAP', '2019-02-27 08:54:08'),
(52, 10, 1201153537, 'TAP', '2019-02-28 02:46:04');

-- --------------------------------------------------------

--
-- Table structure for table `asisten`
--

CREATE TABLE `asisten` (
  `nim` int(11) NOT NULL,
  `nama_asisten` varchar(100) NOT NULL,
  `kode_asisten` varchar(3) NOT NULL,
  `angkatan` int(4) NOT NULL,
  `rfid` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asisten`
--

INSERT INTO `asisten` (`nim`, `nama_asisten`, `kode_asisten`, `angkatan`, `rfid`) VALUES
(120900083, 'Royandi', 'ROY', 2015, '4147253717'),
(1201157899, 'Tondi Rastafara', 'TND', 2015, '0000000000'),
(1201174020, 'Nganu Hattaraitanakul', 'NGN', 0, ''),
(1201154363, 'Ajeng Kirana', 'AJE', 2015, '4148852741'),
(1201150305, 'Ayu Saskia Priscilla Harris', 'AYU', 2015, '0307905079'),
(1201150304, 'Desi Lestari', 'ICI', 2015, '1586869522'),
(1201154130, 'Dhialdi Wibi Raditya', 'ENY', 2015, '0353252455'),
(1201154432, 'Dwi Sela Tresnasari', 'SEL', 2015, '4148759605'),
(1201154097, 'Fahmy Habib Hasanudin', 'BIB', 2015, '4146789173'),
(1201154359, 'Fitra Akbarokah', 'APT', 2015, '4148554229'),
(1201154229, 'I Gusti Ngurah Bagus Arya Dananjaya', 'RAH', 2015, '0307032359'),
(1201150295, 'Liza Nafiah Maulidina', 'NAF', 2015, '3801778406'),
(1201154120, 'Muhammad Rakatama Farhan Habibie', 'RKT', 2015, '4148595301'),
(1201154268, 'Muhammad Zakiey Azhar', 'JEK', 2015, '4147082869'),
(1201150013, 'Murazky Hengki Riaja', 'MVP', 2015, '4146914981'),
(1201154463, 'Najah', 'BAA', 2015, '0492506883'),
(1201154433, 'Nancy Putri Ramadhani', 'CAY', 2015, '4148511845'),
(1201154484, 'Novita Permata Sari', 'NOV', 2015, '4145159029'),
(1201154538, 'Olivia Cahyaningdhia Saputri', 'LIV', 2015, '4149054533'),
(1201152534, 'Rahma Fauziyah', 'RFZ', 2015, '4148565109'),
(1201154105, 'Rayhan Muhammad', 'RYN', 2015, '4148308405'),
(1201154338, 'Renata Ganiswara', 'BII', 2015, '4144273637'),
(1201153537, 'Rizal Wardhani', 'BOS', 2015, '4148731509'),
(1201154365, 'Safinah Rizkiyani', 'SFN', 2015, '4147260021'),
(1201154325, 'Siska Febriyanti', 'CYK', 2015, '4145328965'),
(1201164292, 'Amanda Sucita Brahmana', 'AMD', 2016, '4148258997'),
(1201164326, 'Aqfie jumidhal Ardha', 'AFI', 2016, '4147144853'),
(1201164042, 'Arief Adhitya Rahman', 'PEP', 2016, '4146486325'),
(1201164125, 'Dina Anjani', 'SWR', 2016, '4146872309'),
(1201160452, 'Firdausa Ramadhanti', 'UUS', 2016, '4148496133'),
(1201164165, 'Iksan Budiman', 'LVR', 2016, '4148818229'),
(1201164294, 'Indah Rahayu Alika Putri', 'ALK', 2016, '4146537269'),
(1201164318, 'Mochamad Ilham Fanani', 'IFN', 2016, '4145248597'),
(1201164242, 'Mohammad Fuad Farisi', 'AIS', 2016, '4147213365'),
(1201162112, 'Msy Cahaya Dinda Pamungkas', 'MSY', 2016, '0308192535'),
(1201160303, 'Muhammad Yumna Majdina', 'MAJ', 2016, '3286426326'),
(1201164170, 'Naufal Rifat Muhammad Harris', 'RTH', 2016, '4146673525'),
(1201164402, 'Nur Fajar Fadhilah', 'NFF', 2016, '0307668055'),
(1201164137, 'Nur Fitrianita Syahnas', 'PIW', 2016, '4144639765'),
(1201164371, 'Ramadhani Kurniawan', 'RAM', 2016, '0307699255'),
(1201160338, 'Rully Satriawan', 'RUS', 2016, '4147273925'),
(1201164310, 'Wildan Chairul Faqqie', 'DAN', 2016, '4148955573'),
(1201174086, 'Alan Ramadhan', '', 2017, ''),
(1201174200, 'Arifianto Aji Saputro', '', 2017, ''),
(1201174181, 'Dimas Arvian Budianto', '', 2017, ''),
(1201184202, 'Faturochman Nirwana', '', 2017, ''),
(1201174317, 'Fitriya Ayuningtyas Rumanda', '', 2017, ''),
(1201170119, 'Jasmine Devina Fany Atmaja', '', 2017, ''),
(1201174379, 'Kevin Ahmad Aufarrizky', '', 2017, ''),
(1201174051, 'Komang Surya Gitawan Argya Putra', '', 2017, ''),
(1201174075, 'Made Arya Teguh Dvaipayana', '', 2017, ''),
(1201174063, 'Muhammad Arief Ardyansyah', '', 2017, ''),
(1201174084, 'Muhammad Rafif Aliyasyah', '', 2017, ''),
(1201174255, 'Sandy Argya Pradipta', '', 2017, ''),
(1201174313, 'Sholekah Dwi Saputri', '', 2017, ''),
(1201170332, 'Talitha Nursabila', '', 2017, ''),
(1201174240, 'Ulfia Rahmah', '', 2017, ''),
(1201170396, 'Wardatul Faizah', '', 2017, ''),
(1201184077, 'Wina Nisrina Nalini', '', 2018, ''),
(120900084, 'Royandi Jr', 'ROJ', 2015, '4147253717'),
(1201164337, 'Lazuardi Iman Mauludi', 'LIM', 0, '4146810773');

-- --------------------------------------------------------

--
-- Table structure for table `system_menu`
--

CREATE TABLE `system_menu` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `parent_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `id_pm` int(11) NOT NULL,
  `menu_name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `icon` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `menu_order` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `system_menu`
--

INSERT INTO `system_menu` (`id`, `parent_id`, `id_pm`, `menu_name`, `url`, `icon`, `menu_order`) VALUES
(4, 0, 2, 'Cpanel', '', 'fa fa-bars', 4),
(6, 4, 2, 'Akun', 'cpanel/account', '', 3),
(11, 4, 2, 'Hak Akses', 'cpanel/access', '', 0),
(8, 4, 2, 'Menu', 'cpanel/menu', '', 4),
(32, 21, 1, 'Buat Absensi', 'data/absensi', '', 0),
(21, 0, 1, 'Manajemen Absensi', '', 'fa fa-database', 1),
(25, 20, 3, 'Manajemen Jadwal', 'registrasi/jadwal', '', 0),
(29, 0, 4, 'Dashboard', 'main/dashboard', 'fa fa-desktop', 0),
(33, 0, 1, 'Manajemen Data', '', '', 2),
(34, 33, 1, 'Asisten Laboratorium', 'datamgt/asisten', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_module`
--

CREATE TABLE `system_module` (
  `id_pm` int(11) NOT NULL,
  `module_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_module`
--

INSERT INTO `system_module` (`id_pm`, `module_name`) VALUES
(1, 'Data Management'),
(2, 'Cpanel'),
(3, 'Registrasi'),
(4, 'Dashboard');

-- --------------------------------------------------------

--
-- Table structure for table `system_user`
--

CREATE TABLE `system_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `display_path` varchar(255) NOT NULL,
  `level` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_user`
--

INSERT INTO `system_user` (`id_user`, `username`, `password`, `display_name`, `display_path`, `level`) VALUES
(1, 'raysoesanto', '938b4263f09b8b1dae8f027d06681ec9', 'Ray Soesanto', '/assets/images/ryp.png', 1),
(2, 'admin', 'c469f799b55a6d6a1b7be275d1459f6f', 'Admin', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `system_user_level`
--

CREATE TABLE `system_user_level` (
  `id_level` int(11) NOT NULL,
  `level_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_user_level`
--

INSERT INTO `system_user_level` (`id_level`, `level_name`) VALUES
(1, 'Super Admin'),
(2, 'Admin '),
(3, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `system_user_privileges`
--

CREATE TABLE `system_user_privileges` (
  `id_up` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_pm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_user_privileges`
--

INSERT INTO `system_user_privileges` (`id_up`, `id_level`, `id_pm`) VALUES
(1, 1, 1),
(2, 1, 2),
(11, 1, 3),
(12, 1, 4),
(13, 1, 5),
(15, 3, 4),
(16, 2, 1),
(17, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `system_user_role`
--

CREATE TABLE `system_user_role` (
  `id_ur` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_aal`
-- (See below for the actual view)
--
CREATE TABLE `view_aal` (
`id_al` int(11)
,`id_absensi` int(11)
,`nim` int(10)
,`status` varchar(100)
,`timestamp` timestamp
,`nama_asisten` varchar(100)
,`kode_asisten` varchar(3)
,`angkatan` int(4)
,`rfid` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_late_asisten`
-- (See below for the actual view)
--
CREATE TABLE `view_late_asisten` (
`nim` int(11)
,`nama_asisten` varchar(100)
,`kode_asisten` varchar(3)
,`angkatan` int(4)
,`rfid` varchar(10)
,`count_late` bigint(21)
);

-- --------------------------------------------------------

--
-- Structure for view `view_aal`
--
DROP TABLE IF EXISTS `view_aal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_aal`  AS  select `a`.`id_al` AS `id_al`,`a`.`id_absensi` AS `id_absensi`,`a`.`nim` AS `nim`,`a`.`status` AS `status`,`a`.`timestamp` AS `timestamp`,`b`.`nama_asisten` AS `nama_asisten`,`b`.`kode_asisten` AS `kode_asisten`,`b`.`angkatan` AS `angkatan`,`b`.`rfid` AS `rfid` from (`absensi_list` `a` join `asisten` `b` on((`a`.`nim` = `b`.`nim`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_late_asisten`
--
DROP TABLE IF EXISTS `view_late_asisten`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_late_asisten`  AS  select `a`.`nim` AS `nim`,`a`.`nama_asisten` AS `nama_asisten`,`a`.`kode_asisten` AS `kode_asisten`,`a`.`angkatan` AS `angkatan`,`a`.`rfid` AS `rfid`,(select count(0) from `absensi_list` `b` where ((`b`.`nim` = `a`.`nim`) and (`b`.`status` = 'LATE'))) AS `count_late` from `asisten` `a` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absensi`);

--
-- Indexes for table `absensi_detail`
--
ALTER TABLE `absensi_detail`
  ADD PRIMARY KEY (`id_ad`),
  ADD KEY `id_absen` (`id_absensi`);

--
-- Indexes for table `absensi_list`
--
ALTER TABLE `absensi_list`
  ADD PRIMARY KEY (`id_al`),
  ADD KEY `id_absensi` (`id_absensi`),
  ADD KEY `id_asisten` (`nim`);

--
-- Indexes for table `asisten`
--
ALTER TABLE `asisten`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `system_menu`
--
ALTER TABLE `system_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_module`
--
ALTER TABLE `system_module`
  ADD PRIMARY KEY (`id_pm`);

--
-- Indexes for table `system_user`
--
ALTER TABLE `system_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `system_user_level`
--
ALTER TABLE `system_user_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `system_user_privileges`
--
ALTER TABLE `system_user_privileges`
  ADD PRIMARY KEY (`id_up`);

--
-- Indexes for table `system_user_role`
--
ALTER TABLE `system_user_role`
  ADD PRIMARY KEY (`id_ur`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `absensi_detail`
--
ALTER TABLE `absensi_detail`
  MODIFY `id_ad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `absensi_list`
--
ALTER TABLE `absensi_list`
  MODIFY `id_al` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `system_menu`
--
ALTER TABLE `system_menu`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `system_module`
--
ALTER TABLE `system_module`
  MODIFY `id_pm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `system_user`
--
ALTER TABLE `system_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_user_level`
--
ALTER TABLE `system_user_level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `system_user_privileges`
--
ALTER TABLE `system_user_privileges`
  MODIFY `id_up` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `system_user_role`
--
ALTER TABLE `system_user_role`
  MODIFY `id_ur` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
